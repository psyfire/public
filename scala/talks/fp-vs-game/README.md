Running this project requires [sbt](https://www.scala-sbt.org/), and an internet connection to download the [ScalaTest](http://www.scalatest.org/) dependencies described in the [build.sbt](build.sbt). 

If you run into difficulties compiling or running this, please contact ![pic](../../../assets/contact.PNG)

## Building
```
# Build Only
sbt compile

# Run all Tests
sbt test
```

## Manual / REPL

```bash
$ sbt console
scala>         import com.psyfire.example.navigation._
scala>         import com.psyfire.example.math._

scala>         val pi = math.Pi.toFloat
scala>         val range30_60 = Range1D(pi/6f, pi/3f)

scala>         val move1 = MoveConf(thrustDist = Range1D(1f,2f), thrustAngle = Range1D(.5f,1f))
scala>         val spatial1 = Spatial(position = Vect2(0f,0f), angle = 0f)

scala>         move1.thrustTowards(Vect2(10f,0f), spatial1)
res0: com.psyfire.example.math.Vect2 = (1.0,0.0)

scala>         move1.thrustTowards(Vect2(0f,10f), spatial1)
res1: com.psyfire.example.math.Vect2 = (0.0,0.0)

scala>         move1.thrustTowards(Vect2(10f,10f), spatial1)
res2: com.psyfire.example.math.Vect2 = (0.4292035,0.0)
```

## The Talk

* [Talk Contents](talk)

## The Game

* https://gitlab.com/psyfire/public/space2d
* Not really "distributable" but available: [HERE](https://drive.google.com/file/d/1nxH4UooNFjITplZXYCcdXfTAW7-r2863/view?usp=sharing)

## Contact

* ![pic](../../../../assets/contact.PNG)
* https://linkedin.com/in/psyfire