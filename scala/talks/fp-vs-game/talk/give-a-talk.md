## Give a Talk!

**Why?**

* Good for career and visibility
* Good skillset & learning opportunity

**Slow Down**

* Avoid "TMI" Too-much-information.
* Cut scope
* Move forward at a steady-pace

**Nervous about...**

* Nervous about Crowds 
    * Mentor 1-on-1 or 1-on-few
    * Host/Give talks to people you know
    * ...or give talks around Strangers.
        * don't have to see them every day
* Nervous about Content
    * Start with what you know.
    * Start with what you're passionate about (incoming rant!?)
* Nervous about Quality/Presentation
    * Practice makes ... iterative improvement!
    * Practice also helps with nerves a LOT!
* Not that scary    

**Where/When?**

* Mentor others 1-on-1 or in a small group
* Team-setting.
* Lightning Talks!
    * Give a short 5-minute talk!  Concise is good!
* Host talks at work, or present during regularly-scheduled talks.
    * ex: "Functional Programming Club"
    * No one will stop you.
* Scala Meetup
    * Just do it! I’m doing it.
    * Cody will appreciate it.
* Advice
    * Interactive
    * Know your audience
    * Accept Temporary “Failure”
    * Prepare!
    * Limit Scope
    * Specific Technical easier than Abstract
    