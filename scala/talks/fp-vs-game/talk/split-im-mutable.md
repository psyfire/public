## Trick: Separate Mutable & Immutable Concerns

When constructing a new instance of a game-object, we might need a lot of parameters, values, or data to maintain.  Our
 first iteration of a class might look something like this:

```scala
class Bullet(
  id: Id,
  world: World, // a global physics world
  velocity: Float,
  size: Vector2,    //how big the bullet is
  damage: Float,    // how much damage to do to a target
  image: Image,     //The image to render
  faction: Faction,
  var health: Health,
  lifetime: Float,
  // ... and more...
) {
  // Happened to notice this var wasn't private, in this early code. D:
  var lifetimeRemaining: Float
  private val body = world.create(...)
}
```
Problems:

* Giant parameter list that is likely to grow over time
* Maintains reference to mutable global world.
* Each reference requires a copy of this data.
* Some objects must be `var` or be able to be mutated.
* Mutable parameters (like health) could be modified elsewhere.
* Testing requires creation of entire object (Bullet)
* Impractical to use case-classes

To address most of the above, we're going to apply a simple trick of packaging most of our data into two classes;
 one containing immutable settings that apply to most instances, and the other containing mutable state. 
```scala
class Bullet(
  conf: BulletConf,
  state: BulletState
)

// Immutable values that don't change from one bullet to the next.
case class BulletConf(
  velocity: Float,
  size: Vector2,    //how big the bullet is
  image: Image,     //The image to render
  damage: Float,    // how much damage to do to a target
  lifetime: Float,
  faction: Faction,
)

// Anything mutable or unique per bullet.
case class BulletState(
  id: Id,
  body: Body,
  var health: Health,
  var lifetimeRemaining: Float
)
```

Advantages:

* We're able to pass around a single immutable instance of `BulletConf`, and don't need to copy the values.
* Mutability is packaged and isolated in one obvious place: `state: BulletState`.
* Able to use `case classes` for the Settings and Config classes, gaining benefits like the `copy` method.
* The `Config` and `State` classes have smaller and more manageable parameter lists that have a better logical structure.
* Even with mutability, this is more testable due to being able to define a state & conf, perform an action, and then expect a resulting state.