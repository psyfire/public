## Spaceship Thrust Example

Given a `PlayerActor` and an `EnemyActor`, how would the EnemyActor target, fly towards, and shoot at the player?

The initial ball-of-mud might try to do all-the-things in a single function.  How can we break this down?

## Breakdown

* Global: Scan area for target
    * Reads global state.  Could also break this down into pure functions, but ignoring for now.
* Calculate Spatial Data
    * Impure: For self & target, calculate immutable Spatial data.
* AI Target Enemy
    * Pure: Calculate target-leading position from self & target Spatial data.
    * Pure: Calculate if weapon should be active.
        * Side effect: Shooting the weapon is a side-effect.  Either return value, or move outside function.
* AI Calculate Movement Intent
    * Pure: AI Calculate intended steering left/right (-1 to +1)
    * Pure: AI Calculate intended movement forward/back/left/right (-1 to +1 for X & Y Axis)
* Engines Calculate Thrust
    * Pure: Calculate torque from intended steering left/right.
    * Pure: Calculate linear force from intended forward/back/left/right movement.
* Apply Thrust
    * Side effect: Apply thrust and torque to the physics body.


## Sample Code

* Example starts with [WaypointNav](../src/main/scala/com/psyfire/example/navigation/WaypointNav.scala)
* This example doesn't demonstrate the entire outline above, but we can see components of it.
* [Components are testable.](../src/test/scala/com/psyfire/example/navigation) 