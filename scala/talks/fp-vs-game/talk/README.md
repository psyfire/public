# Functional Programming in a Dysfunctional Game World

## Me

* Jonathan Griffin
* <3 FP & Scala
* Started giving PF talks at work 1-year ago: FP in Scala Book, Covariant vs Contravariant, Collaborative improve code
* Background: Math, 3D, Design (Art), and (of course) Programming
* Love playing Video Games & making them too!

## Philosophical

* Slightly abstract and philosophical
* VERY different from our standard Scala Meetup.
* Experimental Talk - Discover harder to prep than a standard tech-talk
* Don't expect to learn about Monads, Covariant vs Contravariant, ScalaZ, Cats.
    * This is not that talk. Probably do a technical FP talk later.
* Ideally: Insightful, Different, Interesting, Fun, Unintentionally Funny
* Applicable outside games
* Discussion oriented. Please interrupt: paper airplanes, hand signals, vocal (if not looking).

## The Game

* Spaceship RPG, 2D, Action, Physics Based
* LibGDX
    * Tooling for OpenGL, Box2D (physics), Desktop/Android/IOS/etc.
    * Java. Very not-FP
    * More a Game-Framework than a Game-Engine.
* Intentionally simple so far.
* Long ways from being done.
* Talk not about the game.  Stay after if interested.

## Chaotic World

* Game worlds are VERY chaotic
* Ideal Game: open world, physics, everything interactive, chaos/unpredictability, side-effects everywhere
* Seemingly Radical Opposite of Pure FP
* More challenging to achieve Purity than typical web/business app.
* Tempting “FP is useless/impractical for games!”
    * The real world is far more chaotic and unpredictable than video-games
    * Physics, Chemistry, Math, Science, etc useful despite chaotic world

## Challenges

* Lack of Precedence, Unexplored Territory.
* I’ve made many mistakes
* Landmines because Scala/Java
    * Type Coercion
    * Immutability => copy, memory concerns
    * Garbage Collection
    * Performance
* I'm not an expert

## Advantages

* Components are far more testable
* Cleaner code
* Fewer bugs and unanticipated side-effects.
* Massive learning experience
* *(more on this later)*

##  Definitions

* **Pure Function:** [1] For the same arguments, the same result is always returned. [2] Evaluation has no side-effects. 
* **Referential Transparency:** An expression is called referentially transparent if it can be replaced with its corresponding value without changing the program's behavior.
* **Immutable Object:** an object whose state cannot be modified after it is created.
* **"Impurity"** Functions/methods that are not pure, or objects that are mutable.
* **"Purity"** The pursuit of Pure Functions and Immutability.

## Example: Separate pure functions and side effects

* 100 actors all side-effect-ing eachother might be hard to test
* The core business logic should fit in a pure function and be easy to test
* [Example thrust](example-ai-navigation.md)

## FP Purity Paralysis

* "It's a trap!"
    * Can paralyze your progress by always needing the perfect Pure FP solution
    * Simply won't know the pure solution(s)
* Git 'er Done
    * Get something running first
    * Get your hands dirty, get your code dirty/impure
    * Don’t “Analysis Paralysis”
    * If a mechanism for writing pure FP is not obvious, start with an implementation that is somewhat obvious.
    * Add a few tests
* Iterate towards "more pure FP"
    * Extract pure functions
    * Refactor for immutability
    * IO & State Monad

## FP Purity is a Spectrum

* Purity exists, there are true "Pure Functions" and "Immutable Objects"
* Impurity is a Spectrum.
* Wide range from careless side-effects and mutability everywhere, to carefully restricting mutability and side-effects.
* Often very achievable to be pure within a context
* More pure = more testable, fewer (no) mocks, fewer unpredictable behaviors (i.e. side-effects)
* Example: Future 
    * [Why impure?](https://www.reddit.com/r/scala/comments/3zofjl/why_is_future_totally_unusable/cyns21h?utm_source=share&utm_medium=web2x)
    * Able to use "FP Toolbox" including flatMap, for-yield, etc.
    * Far more testable than imperative equivalent.
* Example: Cache
    * [Cache](../src/main/scala/com/psyfire/example/cache/Cache.scala)
    * [CacheTest](../src/test/scala/com/psyfire/example/cache/CacheTest.scala)

### Separate Pure & Impure

* [Separate mutable and immutable state](split-im-mutable.md)
* Refactor pure functions from impure functions.

## IO Monad and State Monad

Each of the following is a full topic.  Not going to present on these today.  Just know they exist, and are designed for these problems.

**State Monad**

* Alternative to "immutable is hard"
* Instead of mutating state, for a state and a function, returns a computed output and a new state.
* `s -> (a, s)`
* http://eed3si9n.com/learning-scalaz/State.html

**IO Monad**

* Alternative to "no side-effects is hard"
* http://eed3si9n.com/learning-scalaz/IO+Monad.html
   
**Task**

* Alternative to "futures have side-effects & not referentially transparent"
* https://alvinalexander.com/scala/differences-scalaz-task-scala-future-referential-lazy

## Start with the simplest version of `X`.

* What are the minimal parts of `X`, where `X` would not exist without those parts?
* Simplest version is often a small function, with few parameters, and few returned values.
* Consider turning `X` into a trait or abstract class, and extending that for advanced or complex capabilities.

## Failure is okay

* Learn from failure - it’s a step to success
* “Safe failure”
* Keep potential failures bite-sized.  Time-box.
    * Beware highly-complex “FP Masterpieces”
    * Probably a much simpler FP solution you haven’t thought of yet.
    * If your FP solution is more complex, you should maybe discard or stash it, and revisit it later.
    * You’ll probably have a better idea later.
* Be willing to throw it away

## Flag/Mark/Annotate Pure vs Impure

* [Using Annotations](https://alvinalexander.com/scala/scala-impure-annotation-functional-programming-pure-functions)
* Extends Mutable / Immutable
* Comments
* Consistent Naming
* Package name `something.mutable` 

## Create libraries

* Small self contained libraries
* Minimal dependencies
* Might be good candidates for open-sourcing later, or reusable for other projects

## Advantages in Practice

* Pure Functions
    * Always behave in a predictable manner
    * Fix bugs with tests
    * Force naturally cleaner design
    * Significantly Fewer Dependencies
* Immutability
    * Greatly reduces source of bugs
    * Reduces copying
        * With mutable data, you're more likely to copy to avoid bugs and accidental mutation.
    * Less Accidentally Exposing private data.
* Testing
    * Web of side-effects semi-untestable, but components are testable
    * Any component that is immutable/pure is really easy to test
    * Zero mocks
* Learning Experience
    * Mini-rant about OOP who think they know FP & don't try.
    * Learn from hands-on, getting your hands dirty, trying, and trying again.

## Give a Talk!

[Give a talk!](give-a-talk.md)

## This is the End, My Friend.

* `<final thoughts>`
* This Talk:
    * https://gitlab.com/psyfire/public/scala/talks/fp-vs-game
* The Game:
    * https://gitlab.com/psyfire/public/space2d
        * Currently very empty, but where more info will be available.
    * Not really "distributable" but available: [HERE](https://drive.google.com/file/d/1nxH4UooNFjITplZXYCcdXfTAW7-r2863/view?usp=sharing)
* Contact:
    * https://linkedin.com/in/psyfire
    * ![pic](../../../../assets/contact.PNG)
* Any Questions?

![Any Questions - David Pumpkins](https://www.rollingstone.com/wp-content/uploads/2018/06/tom-hanks-david-s-pumpkins-halloween-special-ad50ad89-000d-4af5-8146-6c78093cefaf.jpg?crop=940:705&width=940)