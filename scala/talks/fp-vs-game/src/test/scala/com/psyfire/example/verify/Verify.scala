package com.psyfire.example.verify

import java.io.{PrintWriter, StringWriter}

import org.scalactic.source
import org.scalatest.exceptions.TestFailedException

/**
Warning: Use at your own risk!  I would probably want to significantly re-design this, and reuse parts of ScalaTest before
  actually using this for production.

Example use:
  ```
    import com.psyfire.games.space2d.prototype.Is._
    Verify[(Int,Int),Int]{
      case (a,b) => a + b
    }.expect(
      (2, 2) is 4,
      (1, 2) is 3,
      (3, 3) is 6,
    )

  ```
  */

class Verify[PARAMS,RESULT](function: PARAMS => RESULT) {
  def expect(conditions: Condition[PARAMS,RESULT]*)(implicit pos: source.Position): Verify[PARAMS, RESULT] = {
    val results = conditions.flatMap{case cond => {
      try {
        val result = function.apply(cond.params)
        Some(VerifyResult(cond.params, cond.matcher, Some(result)))
      } catch {
        case e:Throwable => Some(VerifyResult(cond.params, cond.matcher, None, Some(e)))
      }
    }}

    val errors = results.filter(_.failed)
    if (errors.nonEmpty) {
      val msg = errors.map(_.format()).mkString("")
      throw new TestFailedException(
        messageFun = _ => Some(msg),
        cause = None,
        pos = pos,
        payload = None
      )
    }
    this
  }
}

object Verify{
  def apply[PARAMS,RESULT](function: PARAMS => RESULT): Verify[PARAMS,RESULT] =
    new Verify[PARAMS,RESULT](function)
}

trait Matcher[T] {
  def expected: T
  def verify(actual: T): Boolean
  def failureMessage(actual: T): String
}

class Is[T](expectEquals: T) extends Matcher[T]{
  override def expected: T = expectEquals
  override def verify(actual: T): Boolean = {
    actual == expected
  }
  override def failureMessage(actual: T): String = {
    s"$actual did not equal $expected"
  }
}

object Is{
  def apply[T](expected: T) = new Is[T](expected)
  implicit class ExtendedIs[PARAMS](params: PARAMS) {
    def is[RESULT](expected: RESULT): Condition[PARAMS,RESULT] = {
      Condition(params, Is(expected))
    }
  }
}

class IsFloat(tolerance: Float)(expectEquals: Float) extends Matcher[Float]{
  override def expected: Float = expectEquals
  override def verify(actual: Float): Boolean = {
    (actual == expected) ||
      ((actual <= expected + tolerance) && (actual >= expected - tolerance))
  }
  override def failureMessage(actual: Float): String = {
    s"$actual did not equal $expected"
  }
}

object IsFloat{
  def apply(tolerance: Float)(expectEquals: Float): IsFloat = new IsFloat(tolerance)(expectEquals)
  implicit val ft: FloatTolerance = FloatTolerance(0.000001f)
  implicit class ExtendedIsFloat[PARAMS](params: PARAMS) {
    def isFloat(expected: Float)(implicit tolerance: FloatTolerance): Condition[PARAMS,Float] = {
      Condition(params, IsFloat(tolerance.value)(expected))
    }
  }
}

case class FloatTolerance(value: Float)

/** The result of verifying a condition. */
case class VerifyResult[PARAMS,RESULT](
  params: PARAMS,
  matcher: Matcher[RESULT],
  actual: Option[RESULT] = None,
  exception: Option[Throwable] = None,
) {
  lazy val passed: Boolean = actual.exists(matcher.verify)
  lazy val failed: Boolean = !passed
  private lazy val failMsg = actual.map(matcher.failureMessage).getOrElse(exception.map(_.getMessage).getOrElse("Unknown Result"))
  def format(): String = {
    if (!passed) formatFailure else ""
  }
  private lazy val formatFailure: String = {
    val actualMsg = actual.map("  Actual   : " + _.toString).getOrElse("")

    val exceptionMsg = exception.map(e => {
      val sw = new StringWriter
      sw.append("  Exception: ")
      e.printStackTrace(new PrintWriter(sw))
      sw.toString
    }).getOrElse("")

    s"""
       |$failMsg
       |  Params   : $params
       |  Expected : ${matcher.expected}
       |$actualMsg$exceptionMsg
       """.stripMargin
  }
}

/** Represents an input and expected result, when applied to some function being tested.*/
case class Condition[PARAMS,RESULT](
  params:PARAMS,
  matcher: Matcher[RESULT],
  clueMessage: Option[String] = None
) {
  def clue(clueMessage: String): Condition[PARAMS,RESULT] = copy(clueMessage = Some(clueMessage))
}

