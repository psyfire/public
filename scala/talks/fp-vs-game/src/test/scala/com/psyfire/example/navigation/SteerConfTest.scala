package com.psyfire.example.navigation

import com.psyfire.example.verify.Verify
import com.psyfire.example.verify.IsFloat._
import org.scalatest.FunSuite


class SteerConfTest extends FunSuite {
  private val PI = math.Pi.toFloat

  test("raw steer") {
    val conf = SteerConf(angleThreshold = 2f, counterSteerMult = .1f)

    Verify[Float,Float](conf.rawSteer).expect(
      -3f isFloat -1.5f,
      -2f isFloat -1f,
      -1f isFloat -0.5f,
       0f isFloat 0.0f,
       1f isFloat 0.5f,
       2f isFloat 1f,
       3f isFloat 1.5f,
      -1f + 2*PI isFloat  -0.5f,
      -1f - 2*PI isFloat  -0.5f,
       1f + 2*PI isFloat  0.5f,
       1f - 2*PI isFloat  0.5f,
    )
  }

  test("raw counter steer test") {
    val conf = SteerConf(angleThreshold = 2f, counterSteerMult = .1f)

    Verify[(Float,Float),Float]{
      case (angle,angleVel) => conf.rawCounterSteer(angle, angleVel)
    }.expect(
      (-3f,  10f) isFloat  .5f,
      (-2f,  10f) isFloat  0f    clue "Should always be zero, when at Angle Threshold",
      (-1f,  10f) isFloat  -.5f,
      (0f,   10f) isFloat  -1f   clue "Should be `-(angleVel*counterSteerMult)` when pointed at intended angle.",
      (2f,   10f) isFloat  -2f,

      (3f,  -10f) isFloat  -.5f,
      (2f,  -10f) isFloat  0f    clue "Should always be zero, when at Angle Threshold",
      (1f,  -10f) isFloat  .5f,
      (0f,  -10f) isFloat  1f    clue "Should be `-(angleVel*counterSteerMult)` when pointed at intended angle.",
      (-2f, -10f) isFloat  2f,
    )

    val conf2 = SteerConf(angleThreshold = 2f, counterSteerMult = .2f)

    Verify[(Float,Float),Float]{
      case (angle,angleVel) => conf2.rawCounterSteer(angle, angleVel)
    }.expect(
      (-2f,  10f) isFloat  0f,
      (-1f,  10f) isFloat  -1f,
      (0f,   10f) isFloat  -2f,

      (2f,  -10f) isFloat  0f,
      (1f,  -10f) isFloat  1f,
      (0f,  -10f) isFloat  2f,
    )
  }

}
