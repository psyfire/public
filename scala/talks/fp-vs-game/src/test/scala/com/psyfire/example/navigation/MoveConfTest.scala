package com.psyfire.example.navigation

import com.psyfire.example.math.{Range1D, Spatial, Vect2}
import Vect2._
import org.scalatest.FreeSpec

class MoveConfTest extends FreeSpec {
  val pi = math.Pi.toFloat
  val rad15deg = pi/12 // 15 degrees in radians.

  case class given(
    move: MoveConf, position: Vect2 = Vect2.Zero, velocity: Vect2 = Zero, facing: Float = 0f, target: Vect2
  ) {
    lazy val spatial = new Spatial(position = position, velocity = velocity, angle = facing)

    def thrustStabilizeIs(expected: Vect2): Unit = {
      val result = move.thrustStabilize(target, spatial)
      withClue(s"\nActual $result\nExpected $expected\n") {
        assert(result.dist(expected) <= 0.0001f)
      }
    }

    def thrustTowardsIs(expected: Vect2): Unit = {
      val result = move.thrustTowards(target, spatial)
      withClue(s"\nActual $result\nExpected $expected\n") {
        assert(result.dist(expected) <= 0.0001f)
      }
    }
  }

  val range30_60 = Range1D(rad15deg*2, rad15deg*4)

  "MoveConf" - {
    "thrustTowards" - {
      "should only apply forward thrust when facing the target, and reverse when away." in {
        val move = MoveConf(thrustDist = Range1D(0,0.1f), thrustAngle = range30_60)

        given(move = move, target = FRONT) thrustTowardsIs FRONT
        given(move = move, target = LEFT)  thrustTowardsIs Zero
        given(move = move, target = RIGHT) thrustTowardsIs Zero
        given(move = move, target = BACK)  thrustTowardsIs BACK
        given(move = move, target = FRONT * 2 + LEFT)  thrustTowardsIs FRONT // 30 degrees from front
        given(move = move, target = FRONT * 2 + RIGHT) thrustTowardsIs FRONT
        given(move = move, target = BACK  * 2 + LEFT)  thrustTowardsIs BACK
        given(move = move, target = BACK  * 2 + RIGHT) thrustTowardsIs BACK
        given(move = move, target = FRONT + LEFT)  thrustTowardsIs FRONT * .5f // 45 degrees from front
        given(move = move, target = FRONT + RIGHT) thrustTowardsIs FRONT * .5f
        given(move = move, target = BACK  + LEFT)  thrustTowardsIs BACK  * .5f
        given(move = move, target = BACK  + RIGHT) thrustTowardsIs BACK  * .5f
        given(move = move, target = FRONT + LEFT  * 2)   thrustTowardsIs Zero  // 60 degrees
        given(move = move, target = FRONT + RIGHT * 2)   thrustTowardsIs Zero
        given(move = move, target = BACK +  LEFT * 2)    thrustTowardsIs Zero
        given(move = move, target = BACK +  RIGHT * 2)   thrustTowardsIs Zero

      }
    }
    "thrustStabilize" - {
      "should only sideways or reverse velocity component (relative to target) should be canceled, not forwards." in {
        val move = MoveConf(stabilize = 1f)

        given(move = move, velocity = LEFT,         target = FRONT)   thrustStabilizeIs RIGHT
        given(move = move, velocity = RIGHT,        target = FRONT)   thrustStabilizeIs LEFT
        given(move = move, velocity = BACK,         target = FRONT)   thrustStabilizeIs FRONT
        given(move = move, velocity = FRONT,        target = FRONT)   thrustStabilizeIs Zero
        given(move = move, velocity = LEFT + FRONT, target = FRONT)   thrustStabilizeIs RIGHT
        given(move = move, velocity = RIGHT + FRONT,target = FRONT)   thrustStabilizeIs LEFT
        given(move = move, velocity = LEFT + BACK,  target = FRONT)   thrustStabilizeIs FRONT + RIGHT
        given(move = move, velocity = RIGHT + BACK, target = FRONT)   thrustStabilizeIs FRONT + LEFT
      }

      "should counter thrust should be relative to facing direction." in {
        val move = MoveConf(stabilize = 1f)
        val clue1 = "counter thrust should be relative to facing direction."

        (0 to 8).foreach(i => {
          val angle = i * pi / 4f
          val target = FRONT.rotate(angle)
          given(move = move, velocity = LEFT.rotate(angle),  facing = angle, target = target) thrustStabilizeIs RIGHT
          given(move = move, velocity = RIGHT.rotate(angle), facing = angle, target = target) thrustStabilizeIs LEFT
          given(move = move, velocity = BACK.rotate(angle),  facing = angle, target = target) thrustStabilizeIs FRONT
          given(move = move, velocity = FRONT.rotate(angle), facing = angle, target = target) thrustStabilizeIs Zero
          given(move = move, velocity = (LEFT + FRONT).rotate(angle), facing = angle, target = target) thrustStabilizeIs RIGHT
          given(move = move, velocity = (RIGHT + BACK).rotate(angle), facing = angle, target = target) thrustStabilizeIs LEFT + FRONT
        })
      }
      "should correctly handle relative target position." in {
        val move = MoveConf(stabilize = 1f)
        val myPos = Vect2(-3f,-3.5f)
        given(move = move, position = myPos, target = myPos + FRONT * 2f, velocity = FRONT)           thrustStabilizeIs  Zero
        given(move = move, position = myPos, target = myPos + FRONT * 2f, velocity = BACK + RIGHT)    thrustStabilizeIs  FRONT + LEFT
        given(move = move, position = myPos, target = myPos + RIGHT * 1.5f, velocity = BACK + RIGHT)  thrustStabilizeIs  FRONT
      }
    }
  }
}
