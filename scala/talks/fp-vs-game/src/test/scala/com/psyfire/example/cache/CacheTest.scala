package com.psyfire.example.cache

import org.scalatest.FunSuite

class CacheTest extends FunSuite{
  test("Verify basic cache functionality.") {
    /** Mutable value to verify cache updates.*/
    var current: Int = 0
    val writer = Cache(current)
    val reader = writer.reader

    assert(writer.raw.isEmpty,"cache should not update until the value is requested")
    assert(reader() == 0,     "blank Cache should auto-update when read.")
    assert(writer.get() == 0, "blank writer Cache should auto-update when read.")

    current = 1
    assert(reader() == 0,     "Cache shouldn't update until requested.")
    assert(writer.get() == 0, "Cache shouldn't update until requested.")

    current = 2
    writer.clear()
    assert(writer.raw.isEmpty, "Cache should be cleared after `clear()`")
    assert(reader() == 2, "After clear, cache should update when read.")
    current = 3
    writer.clear()
    assert(writer.get() == 3, "Cache should update after clear.")

    current = 4
    assert(writer.refresh() == 4, "Refresh should return new value")
    assert(writer.raw.contains(4), "Refresh should update immediately")

    current = 5 // should be ignored
    writer.refresh()
    writer.set(6)
    assert(reader() == 6, "set should override cache value & ignore updateFn.")
    assert(writer.get() == 6, "set should override cache value & ignore updateFn.")
  }

  test("Verify expiration functionality.") {
    /** Mutable value to verify cache updates.*/
    var lastValue: Int = 0
    var currentValue: Int = 0
    /** Just for test readability*/
    def time(v: Long): Long = v
    def delta(v: Long): Long = v

    val writer = Cache(currentValue)
    val reader = writer.reader

    assert(writer.raw.isEmpty)
    assert(reader() == 0)

    lastValue = currentValue; currentValue = 1
    assert(reader(time(0)) == lastValue)
    assert(reader(time(1)) == currentValue, "Cache should update when version is old")

    lastValue = currentValue; currentValue = 2
    assert(reader(time(2),delta(5)) == lastValue, "Cache should only update when version difference is greater than delta")
    assert(reader(time(6),delta(5)) == lastValue, "Cache should only update when version difference is greater than delta")
    assert(reader(time(7),delta(5)) == currentValue, "Cache should only update when version difference is greater than delta")
  }

}
