package com.psyfire.example.math

object FloatTools {
  implicit class FloatTools(value: Float) {
    /** 1.) Perform a function on the absolute value, then
      * 2.) If value was negative, multiply by -1.*/
    def mapAbs(f: Float => Float, enabled: Boolean = true): Float = {
      if (!enabled) {
        f.apply(value)
      } else {
        val res = f.apply(math.abs(value))
        if (value >= 0) res else -res
      }
    }

  }

}
