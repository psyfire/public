package com.psyfire.example.cache

/**
  * WARNING: This cache is a prototype, use at your own risk.
  * A simplistic cache, which provides similar functionality to a lazy val, except with the added abilities to clear and refresh*/
class CacheWriter[A](updateFn: => A, initialVersion: Long = 0L, defaultExpire: Long = 0L) extends Mutable {
  private var cached: Option[A] = Option.empty
  private var _version = initialVersion

  def raw: Option[A] = cached
  def version: Long = _version

  /** Force an update of the cache immediately, and return the latest value.*/
  def refresh(): A = {
    val next = updateFn
    cached = Some(next)
    next
  }

  def clear(): Unit = {cached = Option.empty}
  def clear(version: Long, maxAge: Long = defaultExpire): Unit = {
    if (version > _version + maxAge){
      _version = version
      clear()
    }
  }
  def get(): A = cached.getOrElse{refresh()}
  /** If `version` is greater than the last cache update, for retrieve of the next cache value.*/
  def get(version: Long, maxAge: Long = defaultExpire): A = {
    if (version > _version + maxAge){
      _version = version
      refresh()
    } else {
      get()
    }
  }

  /** Ignore updateFn and set cache directly.*/
  def set(a: A): Unit = {cached = Some(a)}

  def set(a: A, version: Long): Unit = {
    _version = version
    set(a)
  }
  /** A read-only cache, that is safer to expose.*/
  lazy val reader: Cache[A] = new Cache(this)
}

/** Exposes only read functions for safety.  Does not expose mutation functions like updateCache() or clear()*/
class Cache[A](writer: CacheWriter[A]) {
  def apply(): A = writer.get()
  /** If `version` is greater than the last cache update, for retrieve of the next cache value.*/
  def apply(version: Long, maxDiff: Long = 0L): A = writer.get(version, maxDiff)
}

object Cache {
  def apply[A](updateFn: => A, version: Long = 0L, expire: Long = 1L): CacheWriter[A] = new CacheWriter[A](updateFn, version, expire)
}