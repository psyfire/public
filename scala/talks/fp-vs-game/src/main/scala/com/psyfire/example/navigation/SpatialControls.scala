package com.psyfire.example.navigation

import com.psyfire.example.math.Vect2

/** This can be understood as how the actor intends to navigate, including movement (relative to actor facing) and steering.
  * The action(s) of actually navigating to a target, such using engines and apply thrust, will usually read these values
  * and then apply the action.
  *
  * @param move A Vect2 between -1 & 1, describing the intent to move forward/backwards and left/right (strafe).
  * @param steer A value between -1 & 1, describing the intent to steer right or left.*/
case class SpatialControls (
  move: Vect2,
  steer: Float,
)
