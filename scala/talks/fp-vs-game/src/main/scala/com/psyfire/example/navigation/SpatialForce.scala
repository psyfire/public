package com.psyfire.example.navigation

import com.psyfire.example.math.Vect2


/** The force to be applied to an actor.
  *
  * @param force A Vect2 representing linear force to be applied to the actor.
  * @param torque Rotational force to be applied to an actor.*/
case class SpatialForce(
  force: Vect2,
  torque: Float,
)