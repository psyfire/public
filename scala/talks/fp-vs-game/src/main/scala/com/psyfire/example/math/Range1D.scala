package com.psyfire.example.math

import scala.util.Random

sealed trait RangePt
case object RangeMin extends RangePt
case object RangeMid extends RangePt
case object RangeMax extends RangePt
case object RangeNext extends RangePt
case object RangePrev extends RangePt

/** A 1-dimensional range, from a `min` to `max` value.*/
case class Range1D(min: Float = 0f, max: Float = 0f) {
  if (min>max) {throw new IllegalArgumentException(s"Min is larger than Max: $this")}
  lazy val diff: Float            = max - min
  lazy val mid: Float             = min + diff * 0.5f
  private lazy val prevPt: Float  = min - diff
  private lazy val nextPt: Float  = max + diff
  private lazy val invDiff: Float = if (diff == 0) 0 else 1f/diff // Save calculations on dividing.
  lazy val rangePrev = Range1D(prevPt, min)
  lazy val rangeNext = Range1D(max, nextPt)

  def limit(value: Float, enabled: Boolean = true): Float = {
    if (!enabled) value else {
      if      (value < min) min
      else if (value > max) max
      else    value
    }
  }

  def includes(value: Float): Boolean = {value >= min && value <= max}
  def overlap(other: Range1D): Boolean = {
    if (other.min <= this.min) {
      other.max >= this.min
    } else {
      this.max >= other.min
    }
  }

  /**
    Find the value relative to the min & max.
      f(0.0) = min
      f(0.5) = mid
      f(1.0) = max
      f(2.0) -> max + diff
      f(-1.0) -> min - diff

    `limit = true` => limit the range returned between the minimum and maximum.
    `limit = false` => scale the value over this range, but don't limit.
    `abs = true` => limit the absolute-value of the input, and return the appropriate positive or negative value.
     */

  def relative(value: Float, relativeTo: RangePt = RangeMin, limit: Boolean = false, abs: Boolean = false): Float = {
    import FloatTools._ // TODO why do i have to import here, and not at the top of the class?
    value.mapAbs(v => {
      val rel = if (diff == 0) 0 else {get(relativeTo) + diff * v}
      this.limit(rel, limit)
    }, abs)
  }

  def invRelative(value: Float, relativeTo: RangePt = RangeMin, limit: Boolean = false, abs: Boolean = false): Float = {
    import FloatTools._ // TODO why do i have to import here, and not at the top of the class?
    value.mapAbs(v => {
      val lim = this.limit(v, limit)
      if (diff == 0) {
        if (value < min) 0 else 1
      } else {
        (lim - get(relativeTo)) * invDiff
      }
    }, abs)
  }


  /** rangeAt(0) = current range, rangeAt(1) = next range, rangeAt(-1) = previous range */
  def rangeAt(next: Float): Range1D = {
    if (next == 0 || diff == 0) this else {
      val offset = diff * next
      Range1D(min + offset, max + offset)
    }
  }

  def get(relativeTo: RangePt): Float = relativeTo match {
    case RangeMin => min
    case RangeMid => mid
    case RangeMax => max
    case RangePrev => prevPt
    case RangeNext => nextPt
  }

  /**
  Find where a value is located, relative to the min & max.
      min -> 0.0
      midPoint -> 0.5
      max -> 1.0

    For values outside of min & Max:
      2.0 -> value == min + (max - min)*2
      -1.0 -> value == min + (max - min)*2

    If `center = true` returned values will be scaled between -1 & 1, with 0 being half way between the min & max

    If `limit = true` will ensure returned values are limited between 0.0 & 1.0, or if `center == true` between -1.0 & 1.0
    */

  /** Limits the input to be between the min and max, treating the range line a modulous.  For example MinMaxF(-10, 10).mod(11) would return -9. */
  def mod(value: Float): Float = {
    val m = (value - min) % diff + min
    if (m < min) {m + diff} else {m} // because negative mod :(
  }

  /** Splits the given range into N equal-sized parts.*/
  def splitN(numParts: Int): IndexedSeq[Range1D] = {
    val width = diff/numParts
    var nMin = min
    (0 until numParts).map(_ => {
      val next = nMin + width
      val r = Range1D(nMin, next)
      nMin = next
      r
    })

  }


  def rand(): Float =
    min + Random.nextFloat() * diff
}



object Range1D {
  val range0to1: Range1D = Range1D(0,1)
  val range1to1: Range1D = Range1D(-1,1)
  private val PI = math.Pi.toFloat
  val rangePi: Range1D = Range1D(-PI,PI)
  val range0toPi: Range1D = Range1D(0,PI)
  val range0: Range1D = Range1D(0,0)

  def center(midPt:Float, radius:Float) = Range1D(midPt - radius, midPt + radius)

  def value(minMax: Float): Range1D = Range1D(minMax, minMax)

  implicit def FloatToRange(v: Float): Range1D = Range1D(0, v)
}
