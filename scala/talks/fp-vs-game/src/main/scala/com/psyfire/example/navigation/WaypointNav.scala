package com.psyfire.example.navigation

import com.psyfire.example.math.{Spatial, Vect2}

case class WaypointNav(
  steerConf: SteerConf,
  moveConf: MoveConf,
) {
  def navTo(navWaypoint: Vect2, spatial: Spatial): SpatialControls = {
    val move = moveConf.thrustTowards(navWaypoint, spatial)
    val steer = steerConf.steerTowards(navWaypoint, spatial)
    SpatialControls(move, steer)
  }
}