package com.psyfire.example.math

object SpatialCalc {
  def targetAngle(source: Spatial, target: Vect2): Float = {
    val targetVect = target - source.position
    Range1D.rangePi.mod(targetVect.angleRad - source.angle)
  }
}
