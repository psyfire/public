package com.psyfire.example.navigation

import com.psyfire.example.math.{Range1D, Spatial, Vect2}


trait Engines {
  def steer(rightLeft: Float, angleVel: Float): Float
  def thrust(thrustVector: Vect2, relativeVel: Vect2): Vect2

  def navigate(nav: SpatialControls, spatial: Spatial): SpatialForce = {
    val force = thrust(nav.move, spatial)
    val torque = steer(nav.steer, spatial)

    SpatialForce(force, torque)
  }

  def steer(rightLeft: Float, spatial: Spatial): Float = {
    steer(rightLeft, spatial.angleVel)
  }

  def thrust(thrustVector: Vect2, spatial: Spatial): Vect2 = {
    val relativeVel = spatial.velocity.rotateRad(-spatial.angle)
    thrust(thrustVector, relativeVel).rotateRad(spatial.angle)
  }
}

trait Engine {
  /** For the engine's direction of thrust-force, given
    *  (1) the current velocity in that direction &
    *  (2) the intended thrust (from input, AI) relative to that direction (-1 = full reverse, 0 = no thrust, +1 = full forward)
    *  Calculate the actual thrust-force this engine should apply.  */
  def calcThrust(velocity: Float, thrustIntent: Float): Float
}

/**
  * @param maxThrust the maximum amount of thrust this engine can output.
  * @param velocityLimit the velocity where thrust begins to decrease, to the velocity where thrust is zero.
  * @param restrict limit thrustIntend between 0 and +1. */
case class BasicEngine(maxThrust: Float, velocityLimit: Range1D, restrict: Boolean = true) extends Engine {
  override def calcThrust(thrustIntent: Float, velocity: Float): Float = {
    val limitMult = 1 - velocityLimit.invRelative(velocity, limit = true)
    val desired = Range1D.range0to1.limit(thrustIntent, restrict)
    desired * limitMult * maxThrust
  }
}

/** A basic engine, where thrust declines as the max velocity is approached.
  * Prevents (reduced) actors from going insane speeds in frictionless environments. */
case class BasicEngines (
  thrustUp: Float,
  thrustBack: Float,
  thrustStraffe: Float,
  thrustTorque: Float,
  maxVelocity: Range1D,
  maxRotation: Range1D,
) extends Engines {
  val moveForward = BasicEngine(thrustUp, maxVelocity)
  val moveReverse = BasicEngine(thrustBack, maxVelocity)
  val moveLeft    = BasicEngine(thrustStraffe, maxVelocity)
  val moveRight   = BasicEngine(thrustStraffe, maxVelocity)
  val steerLeft   = BasicEngine(thrustTorque, maxRotation)
  val steerRight  = BasicEngine(thrustTorque, maxRotation)

  /** Calculates torque to apply to the actor. */
  def steer(rightLeft: Float, angleVel: Float): Float = {
    val turnR = steerLeft.calcThrust(rightLeft, angleVel)
    val turnL = steerRight.calcThrust(-rightLeft, -angleVel)
    turnR - turnL
  }

  /** Calculates force to apply to the actor. */
  def thrust(thrustVector: Vect2, relativeVel: Vect2): Vect2 = {
    val up    = moveForward.calcThrust(thrustVector.x, relativeVel.x)
    val down  = moveReverse.calcThrust(-thrustVector.x, -relativeVel.x)
    val left  = moveLeft.calcThrust(-thrustVector.y, -relativeVel.y)
    val right = moveRight.calcThrust(thrustVector.y, relativeVel.y)

    Vect2(up-down,right-left)
  }
}

