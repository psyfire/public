package com.psyfire.example.navigation

import com.psyfire.example.math.{Range1D, Spatial, SpatialCalc, Vect2}

case class MoveConf(
  /** For the distance from the actor to the target, max thrust when `dist >= max` and 0 when dist <= min.*/
  thrustDist: Range1D = Range1D.range0,
  /** For a delta-angle in radians, from the actor's facing direction to it's target.
    Thrust is at max between 0 and +/- minumum, and 0 at maximum angle. */
  thrustAngle: Range1D = Range1D.range0toPi,
  /** A multiplier for the amount of thrust to use to cancel unwanted velocity. */
  stabilize: Float = 0,
) {
  /** Calculate forward thrust only, no stabilization.*/
  def thrustTowards(targetPos: Vect2, spatial: Spatial): Vect2 = {
    val dist = spatial.position.dist(targetPos)
    val relAngle  = math.abs(SpatialCalc.targetAngle(spatial, targetPos)) //abs to ignore left vs right

    val angleFalloff = if (relAngle <= Const.pi_2) {
      1 - thrustAngle.invRelative(relAngle, limit = true)
    } else {// Target behind actor, reverse thrust.
      - 1 + thrustAngle.invRelative(Const.pi - relAngle, limit = true)
    }
    val rangeFalloff = thrustDist.invRelative(dist, limit = true)
    Vect2(x = angleFalloff * rangeFalloff)
  }


  /** Calculate stabalize thrust only.*/
  def thrustStabilize(targetPos: Vect2, spatial: Spatial): Vect2 = {
    val relativePos = targetPos - spatial.position
    val normalizedVel = spatial.velocity.rotateRad(-relativePos.angleRad)
    /** The amount of drift-velocity, relative to the intended target vector.*/
    val driftVelTarget = if (normalizedVel.x > 0) {normalizedVel.copy(x=0)} else normalizedVel

    val actorDriftAngle = driftVelTarget.angleRad + relativePos.angleRad - spatial.angle
    /** Apply thrust in opposite direction of thrift, calculated relative to the actor's facing angle.*/
    Vect2(x = - driftVelTarget.len * stabilize).rotateRad(actorDriftAngle)
  }
}

private[this] object Const {
  val pi = math.Pi.toFloat
  val pi_2 = pi*0.5f
}