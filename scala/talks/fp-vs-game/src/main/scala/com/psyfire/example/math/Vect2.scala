package com.psyfire.example.math

/** An immutable 2D Vector, and math functions around 2D Vectors. */
object Vect2 {
  lazy val X = new Vect2(1, 0)
  lazy val Y = new Vect2(0, 1)
  lazy val Zero = new Vect2(0, 0)
  lazy val FRONT = new Vect2(1, 0)
  lazy val BACK = new Vect2(-1, 0)
  lazy val LEFT = new Vect2(0, 1)
  lazy val RIGHT = new Vect2(0, -1)

  /** Sets this {@code Vect2} to the value represented by the specified string according to the format of {@link #toString()}. */
  def fromString(v: String): Vect2 = {
    val s = v.indexOf(',', 1)
    if (s != -1 && v.charAt(0) == '(' && v.charAt(v.length - 1) == ')') {
      try {
        val x = v.substring(1, s).toFloat
        val y = v.substring(s + 1, v.length - 1).toFloat
        Vect2(x, y)
      } catch {
        case _: Exception =>throw new RuntimeException("Malformed Vect2: " + v)
      }
    } else {
      throw new RuntimeException("Malformed Vect2: " + v)
    }
  }

  def xy(xy: Float) = Vect2(xy,xy)

  object Constants {
    val FLOAT_ROUNDING_ERROR = 0.000001f // 32 bits
    val PI = math.Pi.toFloat
    val radiansToDegrees: Float = 180f / PI
    val radDeg: Float = radiansToDegrees
    val degreesToRadians: Float = PI / 180
  }
}

case class Vect2(x: Float = 0, y: Float = 0) {
  import Vect2.Constants._

  /** Reduce verbosity around either performing an operation if true,
    * or no-op when false*/
  def ifThen(b: Boolean)(v: => Vect2): Vect2 =
    if (b) v else this

  lazy val len2 = x * x + y * y
  lazy val len = math.sqrt(len2).toFloat

  def - (x: Float, y: Float): Vect2 = Vect2(
    this.x - x,
    this.y - y
  )

  def - (v: Vect2): Vect2 = Vect2(
    this.x - v.x,
    this.y - v.y
  )

  lazy val nor: Vect2 =
    ifThen(len2 != 0 && len2 != 1)(
      Vect2(x / len, y / len)
    )

  def + (x: Float, y: Float): Vect2 = Vect2(
    this.x + x,
    this.y + y
  )
  def + (v: Vect2): Vect2 = Vect2(
    this.x + v.x,
    this.y + v.y
  )

  def dot(v: Vect2): Float = dot(v.x,v.y)
  def dot(ox: Float, oy: Float): Float = this.x * ox + this.y * oy

  def * (scalar: Float): Vect2 = Vect2(
    this.x * scalar,
    this.y * scalar
  )

  def * (x: Float, y: Float): Vect2 = Vect2(
    this.x * x,
    this.y * y
  )

  def * (v: Vect2): Vect2 = Vect2(
    this.x * v.x,
    this.y * v.y
  )

  /** Squared distance between this point and that point. */
  def dist2(x: Float, y: Float): Float = {
    val x_d = x - this.x
    val y_d = y - this.y
    x_d * x_d + y_d * y_d
  }

  /** Distance between this point and that point. */
  def dist(x: Float, y: Float): Float = {
    Math.sqrt(dist2(x,y)).toFloat
  }

  def dist2(v: Vect2): Float = dist2(v.x, v.y)
  def dist(v: Vect2): Float = dist(v.x, v.y)

  /** If this vector is longer than the limit, return a truncated vector*/
  def limit(length: Float): Vect2 =  limit2(length * length)
  def limit2(length2: Float): Vect2 =
    ifThen (len2 > length2)(
      this * Math.sqrt(length2 / len2).toFloat
    )

  def clamp(min: Float, max: Float): Vect2 = {
    ifThen(len2 != 0) {
      val max2 = max * max
      if (len2 > max2) {
        this * Math.sqrt(max2 / len2).toFloat
      } else {
        val min2 = min * min
        if (len2 < min2) {
          this * Math.sqrt(min2 / len2).toFloat
        } else {
          this
        }
      }
    }
  }

  def length2(newLen2: Float): Vect2 =
    ifThen(len2 != 0 && len2 != newLen2){
      this * Math.sqrt(newLen2/len2).toFloat
    }

  def length(len: Float): Vect2 = length2(len * len)

  /** Converts this {@code Vect2} to a string in the format {@code (x,y)}. */
  override lazy val toString: String = s"($x,$y)"

  /** 2D cross-product. */
  def crs(x: Float, y: Float): Float = this.x * y - this.y * x
  def crs(v: Vect2): Float = crs(v.x, v.y)

  /** @return the angle in degrees of this vector (point) relative to the x-axis. Angles are towards the positive y-axis
    *         (typically counter-clockwise) and between 0 and 360. */
  lazy val angleDeg: Float = {
    val angle = Math.atan2(y, x).toFloat * radiansToDegrees
    if (angle < 0) angle + 360 else angle
  }

  /** @return the angle in degrees of this vector (point) relative to the given vector. Angles are towards the positive y-axis
    *         (typically counter-clockwise.) between -180 and +180 */
  def angleDeg(reference: Vect2): Float = Math.atan2(crs(reference), dot(reference)).toFloat * radiansToDegrees

  /** @return the angle in radians of this vector (point) relative to the x-axis. Angles are towards the positive y-axis.
    *         (typically counter-clockwise) */
  lazy val angleRad: Float = Math.atan2(y, x).toFloat

  /** @return the angle in radians of this vector (point) relative to the given vector. Angles are towards the positive y-axis.
    *         (typically counter-clockwise.) */
  def angleRad(reference: Vect2): Float = Math.atan2(crs(reference), dot(reference)).toFloat

  /** Sets the angle of the vector in degrees relative to the x-axis, towards the positive y-axis (typically counter-clockwise).
    *
    * @param degrees The angle in degrees to set. */
  def setAngleDeg(degrees: Float): Vect2 = setAngleRad(degrees * degreesToRadians)

  /** Sets the angle of the vector in radians relative to the x-axis, towards the positive y-axis (typically counter-clockwise).
    *
    * @param radians The angle in radians to set. */
  def setAngleRad(radians: Float): Vect2 =
    Vect2(len, 0f).rotateRad(radians)

  /** Rotates the Vect2 by the given angle, counter-clockwise assuming the y-axis points up. */
  def rotateRad(radians: Float): Vect2 = {
    val cos = Math.cos(radians).toFloat
    val sin = Math.sin(radians).toFloat
    Vect2(
      this.x * cos - this.y * sin,
      this.x * sin + this.y * cos
    )
  }

  /** Rotates the Vect2 by the given angle in radians, counter-clockwise assuming the y-axis points up. */
  def rotate(radians: Float): Vect2 = rotateRad(radians)

  /** Rotates the Vect2 by the given angle, counter-clockwise assuming the y-axis points up. */
  def rotateDeg(degrees: Float): Vect2 = rotateRad(degrees * degreesToRadians)

  /** Rotates the Vect2 by the given angle around reference vector, counter-clockwise assuming the y-axis points up.
    *
    * @param degrees   the angle in degrees
    * @param reference center Vect2 */
  def rotateAroundDeg(reference: Vect2, degrees: Float): Vect2 =
    (this - reference).rotateDeg(degrees) + reference

  /** Rotates the Vect2 by the given angle around reference vector, counter-clockwise assuming the y-axis points up.
    *
    * @param radians   the angle in radians
    * @param reference center Vect2 */
  def rotateAroundRad(reference: Vect2, radians: Float): Vect2 =
    (this - reference).rotateRad(radians) + reference

  lazy val rot90: Vect2 = Vect2(-y, x)
  lazy val left: Vect2 = rot90
  lazy val rot270: Vect2 = Vect2(y, -x)
  lazy val right: Vect2 = rot270
  lazy val rot180: Vect2 = Vect2(-x, -y)
  lazy val back: Vect2 = rot180

  def lerp(target: Vect2, alpha: Float): Vect2 = {
    val invAlpha = 1.0f - alpha
    Vect2(
      (x * invAlpha) + (target.x * alpha),
      (y * invAlpha) + (target.y * alpha)
    )
  }

  def ~= (other: Vect2, epsilon: Float = FLOAT_ROUNDING_ERROR): Boolean = {
    epsilonEquals(other, epsilon)
  }

  /** Compares this vector with the other vector, using the supplied epsilon for fuzzy equality testing.*/
  def epsilonEquals(other: Vect2, epsilon: Float = FLOAT_ROUNDING_ERROR): Boolean = {
    (Math.abs(other.x - x) < epsilon) && (Math.abs(other.y - y) < epsilon)
  }

  lazy val isUnit: Boolean = isUnit(0.000000001f)
  def isUnit(margin: Float): Boolean = Math.abs(len2 - 1f) < margin

  lazy val isZero: Boolean = x == 0 && y == 0
  lazy val nonZero: Boolean = !isZero
  def isZero(margin: Float): Boolean = len2 < margin

  def isOnLine(other: Vect2): Boolean = isNearZero(x * other.y - y * other.x)
  def isOnLine(other: Vect2, epsilon: Float): Boolean = isNearZero(x * other.y - y * other.x, epsilon)

  def isCollinear(other: Vect2, epsilon: Float): Boolean = isOnLine(other, epsilon) && dot(other) > 0f
  def isCollinear(other: Vect2): Boolean = isOnLine(other) && dot(other) > 0f

  def isCollinearOpposite(other: Vect2, epsilon: Float): Boolean = isOnLine(other, epsilon) && dot(other) < 0f
  def isCollinearOpposite(other: Vect2): Boolean = isOnLine(other) && dot(other) < 0f

  def isPerpendicular(vector: Vect2): Boolean = isNearZero(dot(vector))
  def isPerpendicular(vector: Vect2, epsilon: Float): Boolean = isNearZero(dot(vector), epsilon)

  def hasSameDirection(vector: Vect2): Boolean = dot(vector) > 0
  def hasOppositeDirection(vector: Vect2): Boolean = dot(vector) < 0


  private def isNearZero(value: Float, tolerance: Float = FLOAT_ROUNDING_ERROR): Boolean = Math.abs(value) <= tolerance


}