package com.psyfire.example.math

/** Location, angle, and velocity info.
  * Used as an immutable representation of the Actor's Body's location, without exposing the mutable Body. */
case class Spatial(
  position: Vect2 = Vect2.Zero,
  velocity: Vect2 = Vect2.Zero,
  angle: Float    = 0f,
  angleVel: Float = 0f,
  size: Vect2     = Vect2.Zero
) {
  def print: String = s"{pos:$position, velocity:$velocity, angle:$angle, angleVel: $angleVel}"

  /** The child spatial is relative to the parent.  The returned value is the absolute Spatial.
    * For example `spaceshipSpatial offset bulletSpatial` would return the bullet's velocity & position in world coordinates.*/
  def offset (child: Spatial) = Spatial(
    position = this.position + (child.position.rotateRad(this.angle)),
    velocity = this.velocity + (child.velocity.rotateRad(this.angle)),
    angle    = this.angle    + child.angle,
    angleVel = child.angleVel,  // Do not want to inherit parent's rotational velocity.
    size     = child.size       // Do not want to inherit parent's size.
  )
}


object Spatial{
  val EMPTY = Spatial()
}
