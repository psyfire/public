package com.psyfire.example.navigation

import com.psyfire.example.math.{Spatial, SpatialCalc, Vect2}
import com.psyfire.example.math.Range1D._

case class SteerConf(
  angleThreshold: Float,
  /** How powerful the counter-steer should be.  A good starting value is probably `1/maxAngleVelocity`*/
  counterSteerMult: Float
) {
  if (angleThreshold <= 0) throw new IllegalArgumentException("Threshold must be greater than zero.")

  private lazy val angleMult = 1f/angleThreshold

  def rawSteer(relativeAngle: Float): Float = {
    val normalized = rangePi.mod(relativeAngle)
    normalized * angleMult
  }

  def rawCounterSteer(relativeAngle: Float, angularVel: Float): Float = {
    if (counterSteerMult == 0 || angularVel == 0) {
      0
    } else {
      val normalizedAngle = rangePi.mod(relativeAngle) * angleMult
      val adjustedVel = angularVel * counterSteerMult
      if (angularVel > 0) {
        - (normalizedAngle + 1) * adjustedVel
      } else {
        (normalizedAngle - 1) * adjustedVel
      }
    }
  }

  /** Steer towards a given target.  Value between +1 & -1 indicates thrust left-rotation or right-rotation.*/
  def steer(relativeAngle: Float, angularVel: Float): Float = {
    val rawSteer    = this.rawSteer(relativeAngle)
    val rawCounter  = this.rawCounterSteer(relativeAngle, angularVel)
    range1to1.limit(rawSteer + rawCounter)
  }

  def steerTowards(target: Vect2, spatial: Spatial): Float = {
    val relAngle  = SpatialCalc.targetAngle(spatial, target)
    steer(relAngle, spatial.angleVel)
  }
}
