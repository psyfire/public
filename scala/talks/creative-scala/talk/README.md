# Creative Scala

A light talk about Creative Programming in Scala.

## Intro

* [Why Be Creative?](intro/WhyCreative.md)
* [Why NOT Be Creative?](intro/WhyNotCreative.md)

## Basic Tips

* [Have Fun](tips/HaveFun.md)
* [Creativity Muscle](tips/CreativeMuscle.md)
* [Start Tiny](tips/StartTiny.md)
* [Fail Small & Scrap It!](tips/ScrapIt.md)
* [Self Contained](tips/SelfContained.md)
* [Don't be Clever](tips/DontClever.md)
* [How to (Not) Annoy Coworkers](tips/AnnoyingCoworkers.md)
* [Build As Needed](tips/BuildTheNeeedful.md)
* [Scala Tips](tips/ScalaTips.md)

## Psuedocode Driven Development

[PsuedoCode Driven Development](PsudeoDrivenDevelopment.md)

## Examples

[Examples](../Examples/README.md)
