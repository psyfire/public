## Don't be Clever

Your target audience is a cave-man.  That cave-man may be future-you.

Overly-clever solutions are almost always...

* Scrapped eventually
* Hard to write
* Hard to maintain
* Hard to use
* A MUCH simpler solution is discovered later.

Clever tends to result in going down frustrating rabbit holes, ruins fun, and kills creativity.

If it's too clever, maybe write some notes and psuedo-code, sleep on it, and come back.
