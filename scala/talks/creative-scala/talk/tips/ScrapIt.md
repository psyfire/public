
### Fail Small & Scrap It!

* "Failure" is part of the creative process & not a waste.
    * Helps you come up with better solutions
    * "Sleep on it!"
* Expect to completely scrap or rewrite it at least once.
* Don't overthink the perfect solution the first time.
* Throw it Away!
    * Don't become attached.  Don't hoard.
    * You will have better ideas

**Extra Emphasis:**

"Failure" is part of the creative process.