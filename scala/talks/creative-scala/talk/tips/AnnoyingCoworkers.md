## How to (Not) Annoy Coworkers

* Talk to your coworkers
    * See what they think of the psuedo-code
    * Gather their feedback and ideas before creating
* Document - A little bit of documentation can go a long way.
    * "What is X?"
    * "Why does X exist?"
    * "How do you use X?"
* Be Your Own Best Coworker
    * Code-Review Your Own Code
    * TINY Innovations are easier to Understand, Write, and Justify
    * Creative, not Clever
    * Don't take feedback personally.
* Address real problems
    * Adding business value makes it an easy sell to programmers and non-programmers
* No Luck?
    * Found something you'd enjoy on your own time.
    * Tests (ex: unit-tests) are a great place to practice.
    