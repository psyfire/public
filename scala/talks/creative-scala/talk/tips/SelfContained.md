### Self Contained

When Practical:

* Use the least amount of internal and external dependencies as sensible.
* No dependencies makes it easy to import and re-use.
* No dependencies avoids tangled code-cobwebs
* Consider making it a mini-library, or part of a small library