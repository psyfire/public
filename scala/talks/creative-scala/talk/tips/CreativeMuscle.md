## Creativity Muscle

* Creativity is like a muscle.
* The more you use it, the better you get.
* The less you use it, the more you stagnate.
* Saying "I'm not creative" is like saying "I'm not buff".  Maybe you just need to work out more.
