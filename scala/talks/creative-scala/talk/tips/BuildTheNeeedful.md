## Build as Needed

* Start a new branch (remember "Scrap It")
* Implement the TINIEST version first.
    * Don't implement that whole psuedo-code thing
* Use it in 1-2 places
    * Don't replace every place in your app just yet.
* Test it

**How did it go?**

* **Amazing**
    * That's awesome
    * Continue using it elsewhere in the code
    * Add those other features as you need them.
* **Okay or Mostly Good**
    * Don't be too attached.
    * Is it valuable?
        * Beware you'll probably replace it later.
        * Use it if it won't create a mess & can be replaced.
    * Maybe sleep on it or table it.
    * Judgement Call

* **Bad**
    * You created a branch right?
    * This is part of the creative process.
    * Remember: "Scrap it!"