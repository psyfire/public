## Scala Specific Tips

### `???` Is Your Friend

1. Write Scala
2. `???`
3. Profit!

You can stub out methods super easy

```
case class SuperAwesome(
  thing: String
) {
  def moreAwesome(howAwesome: Double): Int = ???
}

class SuperAwesomeTest extends AnyFreeSpec {
  "Thing Test" - {
    "should do things" in {
      val awesomeA = SuperAwesome("aaay!")
      val awesomeB = SuperAwesome("to bee, or not to sting")
      
      val res1 = awesomeA.moreAwesome(3.1415)
      val res2 = awesomeB.moreAwesome(9001)
      
      val totalAwesome: Int = res1 + res2
    }
  }
}
```

Compiles, but it'll fail when it hits the stubbed line.
```
an implementation is missing
scala.NotImplementedError: an implementation is missing
```


### Scala is Super Flexible

You can go nuts with syntax in scala.  Try to be sensible in how you go nuts.

```
import org.scalatest.freespec.AnyFreeSpec

case class Thing(a: String) {
  val + : add.type = add
  object add{
    def apply(other: Thing): Thing = Thing(a + " : " + other.a)
    def apply(otherRaw: String): Thing = add(Thing(otherRaw))
    def apply(value: Int): Thing = add("value = " + value)
  }

  def when(cond: Boolean) = Thing.When(this, cond)
}

object Thing {
  case class When(thing: Thing, cond: Boolean) {
    val + : add.type = add
    object add{
      def apply(other: Thing): Thing = if(cond) thing.add(other) else thing
      def apply(other: String): Thing = if(cond) thing.add(other) else thing
      def int(value: Int): Thing = if(cond) thing.add(value) else thing
    }
  }
}

class ThingTest extends AnyFreeSpec {
  "Thing Test" - {
    "should do things" in {
      val thingA = Thing("a")
      val thingB = Thing("bb")
      val thingC = Thing("ccc")

      assert(thingA + thingB + thingC + "hello" == Thing("a : bb : ccc : hello"))
      assert(thingC + 5 == Thing("ccc : value = 5"))

      assert(thingA.when(5 < 10).add("yes") == Thing("a : yes"))
      assert(thingA.when(5 > 10).add("no") == Thing("a"))
    }
  }
}

```

