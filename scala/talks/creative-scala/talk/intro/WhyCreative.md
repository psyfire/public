## Why Be Creative?

* **Creativity Muscle** - The more you do it, the better you get.
* **Stronger Skillset** - Forces you to explore paths you travel less, causes you to discover more tools and techniques.
* **Portfolio & Open Source** - Great for sharing with prospective employers, or sharing with others who might find it useful.
* **Interesting Problems** - Many of us started programming because we enjoy solving the interesting problems.