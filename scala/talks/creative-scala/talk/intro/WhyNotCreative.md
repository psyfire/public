## Why NOT Be Creative?

* **Annoy Coworkers** - Alternatively, this is a reason to be creative.
* **Rabbit Holes** - It's easy to go down rabbit holes. Your estimate-vs-reality problem will be much worse with creative tasks  
* **Already Exists a Library** - There may already exist a library that solves the given problem, and solves it better than you would.

**However...**

* These are all reasons to be *wise* about your creative pursuits!
