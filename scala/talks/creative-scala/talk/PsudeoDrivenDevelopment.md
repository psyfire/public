## PsuedoCode Driven Development

Like TDD, but less boring.

* STOP!! Don't write code (yet)
* Before you implement anything, **write out psuedo code of how it'll actually be used first.**
* Write use-cases
    * Do you have actual use-cases already?  What would your prod-code look like instead using this?
    * If you don't have use-cases yet, try to think of some scenarious where you will use it.
    * Finally a few random branstormed peanut-galazy examples to test the limits.

  * This is an actual 
* Purpose of this is to anticipate future need

*(also known as PCP: Psudeo Code Driven Programming)*

## Example: Check Permissions

["Check Permissions" Example](../../examples/CheckPermissionBrainstorm.md)

Here's a PDDD (PsuedoCodeDrivenDevelopmentDocument) that was actually created to prototype out psuedo-code for nicer to use permissions model.

I have a previous iteration of this, which is very useful, but has crappier syntax & is hard to remember how to use it (see the bottom of the doc).