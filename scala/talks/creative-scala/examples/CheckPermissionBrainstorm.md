## Psuedo API

```
CheckRole. <builder>
    .<<what>>
    .<<who>>
    .<<can>>        //repeatable
    .<<final>>


<<Where / What>>
    .zone(...) | .zones(...) | .of[A](list: Seq[A])(a => zoneId)
            // ^ first generic [A].  i.e. [Zone, Target, Tag]

<<Who>>
    .user(...) | .users(...) | .role(...) | .roles(...) | .allRoles() | .public() | .currentUser()

<<Can>>
    .can(Action->Level, ...)
    .canMod <one of>
        .users(...)
            .(Action,...)
            .levels(Action->Level, ...)
        .roles(...)(Action->Level, ...)
            .(Action,...)
            .levels(Action->Level, ...)
        .(Action,...)
        .levels(Action->Level, ...)

<<Final>>
    .result. <one of>       // all raw data
        .errors: Seq[(Action,Level,User,Zone)]
        .getValid: Seq[A]
        .getInvalid: Seq[A]
        .split: (Seq[A], Seq[A])
        .<<Final Action>>               // perhaps we'll want to do multiple things with the data.
    .getValidAndLog(invalid=>Msg)   // verbose name?
    .onError. <one of>
        .autoError()
        .autoLog()
        .log(Msg)
        .logAnd(Msg) : <<Final>>  //continue chaining
        .error(invalid=>ERROR)
    .either[B](valid: Seq[A] => B, invalid: Seq[Errors] => B)
    .filter[B](list): Seq[B]
    .split[B](list): (valid: Seq[B], invalid: Seq[B])
```

## Examples:

Ex: For a search results, filter results by view-permissions

```
searchResults: Seq[SearchR]
viewable <- CheckRole
    .of(searchResults)(_.zoneId)
    .currentUser()
    .can(Content->View)
    .getValid

```

Ex: Check if user can tag several pieces of content.  Log invalid & return valid list

```

content: Seq[ContentTags]
valid: Seq[ContentTags] <- CheckRole
    .of(content)(_.zoneId)
    .currentUser()
    .can(Tag->Create)
    .log(invalid=>msg)
    .getValid

```

Ex: Split list of zones into can/can't Mod

```
zones: Seq[Zone]
(canMod, cantMod) <- CheckRole
    .zones(zoneIds)
    .currentUser()
    .can(Base->MOD)
    .zones.split(zones)(_.zoneId)

```

Ex: Attempt to moderate another user's tags, and generate error if can't moderate.

```
_ <- CheckRole
    .zone(zoneId)
    .currentUser()
    .canMod.user(user2)
        .actions(TAG)
    .autoError() //feeling lazy

```

ex: Attempt to promote another user to a mod

```
_ <- CheckRole
    .zone(zoneId)
    .currentUser()
    .canMod(user2)
        .levels(SET_ROLE->MOD)
    .log(_=>"Cannot Promote $User to $Role")

```

ex: (actual) Create Content permission check

```
_ <- CheckRole.zone(zoneId).currentUser().can(Content->Create).error(...)
```

ex: (actual) Verify user can search tags of requested list of zones

```
_ <- CheckRole.zone(zoneIds).currentUser().can(Content->View, Tag->View).autoError
```

ex: (actual) Verify can create system tag.

```
_ <- CheckRole.zone(zoneIds).currentUser().can(Content->ADMIN).autoError
```

ex: (actual) Filter list of targets, where user can view tags

```
validT <- CheckRole.of(targets)(_.zoneId).can(Tag->View).getValid
```

ex: (actual) Can update a custom role

```
levelUpdates: Seq[Action->Level]
_ <- CheckRole
    .of(zoneId)
    .currentUser()
    .can(SET_ROLE -> MOD)
    .canMod.role(customRole)(levelUpdates)
    .autoError
    
```

ex: (actual) Can create a custom role

```
_ <- CheckRole
    .of(zoneId)
    .currentUser()
    .can(SET_ROLE->MOD).
    .canMod.levels(levels) 

```

ex: (actual) Check if public can view tags in zone

```
publicViewable: Boolean 
   <- CheckRole.of(zoneId).public().can(Tag->View).bool
```

ex: Check if public can view tags in several zones

```
publicViewable: Map[ZoneId, Boolean]
   <- CheckRole.of(zoneIds).public().can(Tag->View)
         .zoneBools
```

ex: Check if several users can do something

```
publicViewable: Map[UserId, Boolean]
   <- CheckRole.of(zoneId).users(userIds).can(levels).userBools
```

ex: (actual) Can assign another moderator

```
_ <- CheckRole
    .zone(zoneId)
    .current()
    .canMod(user2).action(SET_ROLE)
```

ex: Retrieve results, and then do several checks...
```
res <- CheckRole.zone(zoneIds).users(userIds)
    .can(levels)
    .result

_ <- result.onError.log(_ => msg)
filtered <- result.filter(someList)

```

```
res <- CheckRole
    .zone(zoneIds)
    .users(userIds)
    .can(levels)
    .logAnd(_ => msg)
    .filter(someList)
    
```

### Code being Replaced

This is just here for the talk and isn't on the actual PDD.

```
GetRoleSvc.expect(m.userId, req.target.zoneId)(RoleAction.TAG -> RoleLevel.CREATE).autoError

```
^ Concise, but not very flexible, and hard to remember.

```
    ModerateSvc.moderateUser(zoneId, targetUser).canAssign(addRole)
```

^ Concise but there's also a `ModerateSvc`?

```
def getContent(req: GetContentRequest): TIO[Seq[FullContent]] = {
    for {
      contentFields <- getContentFields(req)
      content <- getContentData(contentFields.map(_.contentTypeId))
      zones = contentFields.map(_.zoneId).distinct
      m <- meta()
      _ <- GetRoleSvc.expect(m.userId, zones)(RoleAction.POST -> RoleLevel.VIEW_ONLY).autoError // TODO  filter viewable content, and return a message for any content we can't view.
    } yield {
      Join(contentFields, content).on(_.contentId, _.contentId).innerValues.map{case (f,c) => FullContent(f,c)}
    }
  }
```
^ Oh look, a TODO!  Programmers are so lazy!  This one included.  Maybe we can make this 'TODO' operation trivial

```
    /** Whether or not tags can ve viewed by the public in a zone. */
    def targetPublicViewable(targets: Seq[Target]): TIO[Map[Target, Boolean]] =
      for {
        zoneRoles <- ZoneRoleSvc.zoneRoles.default(targets.map(_.zoneId).distinct)
      } yield {
        val zoneViewable = zoneRoles.byZone.map{case (id,roles) => id -> roles.apply(ZoneRole.NONE).can(RoleAction.TAG -> RoleLevel.VIEW_ONLY)}
        targets.mapTo(t => zoneViewable(t.zoneId))
      }
```
^ I'm going to need to do similar things elsewhere in the code many more times.  We could easily simplify this.

Also, WTF is `ZoneRoleSvc.zoneRoles.default(...)` and how does one remember it?