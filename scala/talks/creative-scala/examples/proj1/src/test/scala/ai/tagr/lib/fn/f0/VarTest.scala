package ai.tagr.lib.fn.f0

import org.scalatest.freespec.AnyFreeSpec

class VarTest extends AnyFreeSpec {
  "Var" - {
    "Var should act like a var" in {
      var mutable = "value1"
      val get = Var(mutable)
      mutable = "value2" // should be ignored
      assert(get() == "value1")

      get.set("value3")
      mutable = "value4" // should be ignored
      assert(get() == "value3")
    }

    "var mutate and self type" in {
      val get = Var(10)
      val get2: Var[Int] = get
        .set(20)
        .mutate(_ + 5)

      assert(get() == 25)
      assert(get2() == 25)

      val opt: MOpt[Int] = MOpt(Some(20))
        .mutate(_.map(_ + 5))
        .innerMutate(_ + 5)

      assert(opt().contains(30))
    }
  }

}
