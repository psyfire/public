package ai.tagr.lib.fn.f0

import org.scalatest.freespec.AnyFreeSpec

class FSetTest extends AnyFreeSpec{
  "FSet" - {
    "F.Set.Var" - {
      "should behave as expected" - {
        var mutable1 = Seq(1,2)
        val set1 = Fn.Set.Var(mutable1)

        mutable1 = Seq(3,4) // should be ignored
        assert(set1() == Seq(1,2))

        set1.set(Seq(5,6))
        mutable1 = Seq(7,8) // should be ignored
        assert(set1() == Seq(5,6))
      }
    }
    "F.Set.Val" - {
      "should act like a val" in {
        var mutable = Seq("v1","v2")
        val get = Fn.Set.Val(mutable)
        mutable = Seq("v3","v4") // should be ignored
        assert(get() == Seq("v1","v2"))
      }
    }
    "F.Set.Lazy" - {
      "should be lazy" in {
        var mutable = Seq("v1","v2")
        val set1: FSet[String] = Fn.Set.Lazy(mutable)
        mutable = Seq("v3","v4")
        assert(set1() == Seq("v3","v4"))
        mutable = Seq("v5","v6")
        assert(set1() == Seq("v3","v4"))
      }
    }
    "F.Set.collect" in {
      var mutable = Seq("value1","key1","other1")
      val set1 = Fn.Set(mutable)
      val set2 = set1.collect{
        case s: String if s.contains("value") => "hello " + s
        case s: String if s.contains("key") => "bye " + s
      }
      assert(set2.contains("hello value1","bye key1"))

      mutable = Seq("value3","key3","other3")
      assert(set2.contains("hello value3","bye key3"))
    }

  }

}
