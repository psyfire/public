package ai.tagr.example.creative

import org.scalatest.Assertions
import org.scalatest.freespec.AnyFreeSpec

import scala.collection.MapView

class Ex2FunClass extends AnyFreeSpec {
  "Fun Class" - {
    "multiple parameter lists?  I think not" - {
      "a def with multiple parameter lists" in {
        def add(value1: Int)(value2: Int): Int = value1 + value2

        assert(add(3)(2) == 5)

        // can't do this
        // val add3 = add(3)

        // slightly annoying syntax, but it works
        val add3 = add(3)_
        assert(add3(2) == 5)

        // but we can't specify parameter names.
        // add3(value2 = 2)
      }

      "replace it with a case class!" in {
        case class add(value1: Int){
          def apply(value2: Int): Int = value1 + value2
        }

        val add3 = add(3)
        assert(add3(10) == 13)
      }


      "lets have more fun!" in {
        case class add(value1: Int){
          def apply(value2: Int): Int = value1 + value2
          def seq(values: Seq[Int]): Seq[Int] = values.map(_ + value1)
        }

        val add3 = add(3)
        assert(add3(10) == 13)

        assert(add3.seq(Seq(1,10,100)) == Seq(4,13,103))
      }

      "ok, but where would I use it?" in {
        import TestUtils._

        def someCalc(value: Double): Double = value / 7.0

        // this is verbose....
        assert(someCalc(3) == 0.42857142857142855)

        // we can do this!
        assert(round2(someCalc(3)) == 0.43)

        //or this!
        val assert3 = assertAprox(3)
        assert3(someCalc(3), 0.429)
      }
    }

    "i herd you liek defs" - {
      val averageSeq = Seq(4,5,6,10,10,12.123)
      "an average function" in {
        import TestUtils._

        // Pretend we're doing some harder calculation.
        def average(seq: Seq[Double]): Double = {
          val sum = seq.sum
          val count = seq.size
          sum / count
        }

        assertD3(average(averageSeq), 7.854)
      }

      "what if something was wrong and we needed to check the intermediate calculations?" in {
        import TestUtils._

        case class average(seq: Seq[Double]){
          lazy val sum: Double = seq.sum
          lazy val count: Int = seq.size
          lazy val average: Double = sum/count
          def apply(): Double = average
        }

        val averaged= average(averageSeq)

        assertD3(averaged(), 7.854)
        assertD3(averaged.sum, 47.123)
        assertD3(averaged.count, 6)
      }

      "why stop there?" in {
        import TestUtils._

        case class Average(seq: Seq[Double]){
          lazy val sum: Double = seq.sum
          lazy val count: Int = seq.size
          lazy val average: Double = sum/count
          def apply(): Double = average

          lazy val sorted: Seq[Double] = seq.sorted
          lazy val median: Double = sorted((count-1)/2)

          lazy val counts: MapView[Double, Int] = seq.groupBy(i => i).mapValues(_.size)
          lazy val mode: Double = counts.maxBy(_._2)._1

          def filter(cond: Double=>Boolean): Average = Average(seq.filter(cond))
        }

        val averaged= Average(averageSeq)

        assertD3(averaged.average, 7.854)
        assertD3(averaged.median, 6.0)
        assertD3(averaged.mode, 10.0)

        assertD3(averaged.filter(_ <=6).average, 5)
      }
    }
  }
}

// this would normally be in some other file.
object TestUtils {
  case class round(decimals: Int) {
    def apply(value: Double): Double =
      BigDecimal(value).setScale(decimals, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

  lazy val round2 = round(2)
  lazy val round3 = round(3)

  case class assertAprox(decimals: Int) {
    val rounded = round(decimals)

    def apply(actual: Double, expected: Double, message: String = ""): Unit = {
      Assertions.assert(rounded(actual) == rounded(expected), message)
    }
  }

  lazy val assertD3 = assertAprox(3)
}