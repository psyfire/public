package ai.tagr.lib.fn.f0

// Inner Type, Self/Return/Map, Filter/Collect Type

// F0[A] => A, A, A, F0[A], Opt[A]
// FOpt[A] => A, Option[A], Opt[A], Opt[A]
// Cache[A] =>  A, A, Cache[A], Opt[A]
// FSet[A] => A. Trav[A] FSet[A], FSet[A]

// F1 => [P,A], P => A, ,
// F1Map => [P,A], Map[P,A],
// F1Set => Trav[

/**
  *
  * @tparam A The generic type of individual elements in the F0 collection.
  * @tparam RETURN The return type for map, flatMap, etc
  */
@deprecated("not in use yet")
trait F0GenOps[A, RETURN[B] <: Fn[B]] {
  /** ----------------------------- Required Implementation  -----------------------------  */
  def elements: TraversableOnce[A]

  def create[B](elements: => TraversableOnce[B]): RETURN[B]

  /** ----------------------------- Map, FlatMap, ForEach  -----------------------------  */
  def map[B](f: A=>B): RETURN[B] = create(elements.map(f))

  def flatMap[B](f: A=> TraversableOnce[B]): FSet[B] = FSet(elements.flatMap(f))

  def exists(p: A => Boolean): Boolean = elements.exists(p)
  def contains[A1 >: A](elem: A1): Boolean = exists(_ == elem)
  def foreach[U](f: A => U): Unit = elements.foreach(f)

  /** ----------------------------- Other  -----------------------------  */
  def find(p: A => Boolean): Opt[A] = Opt(elements.find(p))
  def head(): Opt[A] = find(_ => true)
  def get: A = elements.find(_ => true).get
}

@deprecated("not in use yet")
trait F0Filter[A,RETURN[B]<: Fn[B]] {
  def elements: TraversableOnce[A]
  def createFR[B](elements: => TraversableOnce[B]): RETURN[B]

  def collect[B](pf: PartialFunction[A, B]): RETURN[B] = createFR(elements.flatMap(e => pf.lift(e)))
  def filter(p: A => Boolean): RETURN[A] = createFR(elements.filter(p))

  def filterNot(p: A => Boolean): RETURN[A] = filter(p.andThen(b => !b))
}

@deprecated("not in use yet")
trait F0Convert[A] {
  def apply(): A

  def toOpt: Opt[A] = Opt.some(apply())
//  def toVal: F.Val[A] = F.Val(apply())
//  def toVar: F.Var[A] = F.Var(apply())
//  def toLazy: F.Lazy[A] = F.Lazy(apply())
}