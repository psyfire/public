package ai.tagr.example.creative

case class ExCache[ID,A](idFn: A=>ID) extends (ID=>A) {
  private var data: Map[ID,A] = Map()

  def apply(id: ID): A =
    data(id)

  private val _self = this
  object put {
    def apply(values: A*): ExCache[ID, A] =
      seq(values)

    def seq(values: Seq[A]): ExCache[ID, A] =
      pairs(values.map(v=> idFn(v) -> v) :_*)

    def pairs(values: (ID,A)*): ExCache[ID, A] =
      map(Map(values:_*))

    def map(values: Map[ID,A]): ExCache[ID, A] = {
      data = data ++ values
      _self
    }
  }

  object remove{
    def apply(id: ID): ExCache[ID, A] = {
      data = data.removed(id)
      _self
    }

    def value(value: A): ExCache[ID, A] = {
      remove.where{case (id,a)=> a == value}
    }

    def where(cond: (ID,A)=>Boolean): ExCache[ID,A] = {
      data = data.filter{case (id,a)=> !cond(id,a)}
      _self
    }
  }

  object values{
    def apply(): Iterable[A] = iterator
    lazy val iterator = data.values
    lazy val asSeq = iterator.toSeq
    lazy val asSet = iterator.toSet
    lazy val asMap = data
  }

  object get{
    def apply(id: ID): Option[A] = data.get(id)
    def ids(ids: ID*): Seq[A] = ids.flatMap(data.get)

    def whereId(cond: ID=>Boolean): Iterable[A] = data.filter{case (id,_)=>cond(id)}.values
  }

  case class retrieve(id: ID){
    lazy val value: A = data(id)
    lazy val option: Option[A] = data.get(id)
    lazy val pair: (ID, A) = id -> value
  }
}
