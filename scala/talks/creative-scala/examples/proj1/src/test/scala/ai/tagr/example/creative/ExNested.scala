package ai.tagr.example.creative

import org.scalatest.freespec.AnyFreeSpec

case class Person(
  id: Int,
  name: String
)

class Ex2Nested extends AnyFreeSpec {
  "Fake Cache" - {
    val Seq(p0,p1,p2,p3,p4,p5,p6,p7) =
      Seq(
        0->"Bob",
        1->"Joe",
        2->"Sarah",
        3->"Carl",
        4->"Bob",
        5->"Nick",
        6->"John",
        7->"Cody"
      ).map{case (id,name) => Person(id, name)}

    "This is boring syntax ... but nothing wrong with that" in {
      val cache = ExCache[Int,Person](_.id)

      cache
        .put(p0)
        .put(
          p1,p2,p3,p4,p5,p6
        )

      assert(cache.get(3).get.name == "Carl")

      cache.remove(3)
      assert(cache.get(3).isEmpty)

      assert(cache.values().toSet == Set(p0,p1,p2,p4,p5,p6))
    }




    "...but wait, our cache is a function!" in {
      val cache = ExCache[Int,Person](_.id)
      cache.put(p0,p1,p2,p3,p4,p5,p6,p7)

      val users = Seq(1,3,4).map(cache)
      assert(users == Seq(p1,p3,p4))
    }



    "hold up, how is this happening? " in {
      val cache = ExCache[Int,Person](_.id)

      cache.put.seq(Seq(p0,p1,p2,p3,p4,p5,p6))
      cache.put.pairs(25->p7)
      // ^ wait, put was a method, right?

      val res1: Seq[Person] = cache.get.ids(3,5)
      // ^ now get is `get.ids(...)` which returns a seq?
      assert(res1 == Seq(p3,p5))

      cache
        .remove.where{case (id,person)=> person.name == "Bob"} //removed: p0,p4
        .remove.value(p3)

      assert(cache.values.asMap == Map(1->p1,2->p2,5->p5,6->p6,25->p7))

      assert(cache.retrieve(25).value == p7)
      assert(cache.retrieve(25).pair == 25 -> p7)
    }



  }
}