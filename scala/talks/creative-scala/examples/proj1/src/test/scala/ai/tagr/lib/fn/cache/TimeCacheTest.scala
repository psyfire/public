package ai.tagr.lib.fn.cache

import org.scalatest.freespec.AnyFreeSpec

class TimeCacheTest extends AnyFreeSpec {
  "TimeCache" - {
    def assertIsEmpty[T,U](cache: TimeCache[T,U]): Unit = {
      assert(cache.isEmpty)
      assert(cache.isExpired)
      assert(cache.cachedTime.isEmpty)
      assert(cache.cached.isEmpty)
    }

    /** Retrieves the latest value, and verifies the value returned and updated time. */
    def getAndAssert[T,U](cache: TimeCache[T,U], value:T, time:U, clue:String): Unit = {
      assert(cache() == value, clue)
      assertCache(cache, value, time, clue)
    }

    /** Verifies the cached value and updated time. */
    def assertCache[T,U](cache: TimeCache[T,U], value:T, time:U, clue:String): Unit = {
      assert(cache.cached.contains(value), clue)
      assert(cache.cachedTime.contains(time), clue)
    }

    "should correctly perform basic features" in {
      var mutable:String = "value1"
      var time:Int = 0
      val expire: Expire[Int] = (start,end) => end - start >= 10
      val cache = TimeCache[String,Int](mutable, time, expire)
      assertIsEmpty(cache)

      mutable = "value2"
      time = 5
      getAndAssert(cache, "value2", 5, "should have retrieved a new value")

      mutable = "value3"
      time = 6
      getAndAssert(cache, "value2", 5, "should not have retrieved a new value")
      time = 14
      getAndAssert(cache, "value2", 5, "should not have retrieved a new value")
      time = 15
      getAndAssert(cache, "value3", 15, "should retrieve a new value when expired")
      time = 33
      mutable = "value4"
      getAndAssert(cache, "value4", 33, "should retrieve a new value when expired")

      time = 44
      cache.setCached("value5")
      time = 45
      mutable = "value6"
      getAndAssert(cache, "value5", 44, "should use `set` value")
      time = 54
      getAndAssert(cache, "value6", 54, "when expired, should update")

      mutable = "value7"
      time = 55
      getAndAssert(cache, "value6", 54, "shouldn't update yet")
      cache.clear()
      assertIsEmpty(cache)
      getAndAssert(cache, "value7", 55, "should retrieve a new value when empty")

      mutable = "value8"
      time = 70
      assert(cache.isExpired)
      assertCache(cache, "value7", 55, "should not update on calling `isExpired` ")
      getAndAssert(cache, "value8", 70, "but then update when apply() is called ")
    }
  }
}
