package ai.tagr.lib.join

import org.scalatest.freespec.AnyFreeSpec

case class TestNames(
  id: Int,
  name: String,
)

case class TestEmails(
  userId: Int,
  email: String
)

case class TestNameEmails(
  id: Int,
  name: String,
  email: String
)

object TestNameEmails{
  def apply(testNames: TestNames, testEmails: TestEmails): TestNameEmails = {
    assert(testNames.id == testEmails.userId)
    TestNameEmails(testNames.id, testNames.name, testEmails.email)
  }
}

class JoinTest extends AnyFreeSpec {
  "Join" - {
    val dbNames = Seq(
      TestNames(1, "bob"),
      TestNames(1, "bob2"),
      TestNames(2, "carl"),
      TestNames(3, "joe"),
      TestNames(4, "sue"),
    )

    val dbEmails = Seq(
      TestEmails(1, "bob@bob.com"),
      TestEmails(2, "carl@email.com"),
      TestEmails(3, "joe@joe.com"),
      TestEmails(3, "joe2@joe.com"),
      TestEmails(5, "jim@jimbob.com"),
    )

    val joined = Join(dbNames, dbEmails).on(_.id, _.userId)

    "Inner Join test" in {
      val inner: Set[TestNameEmails] = joined.innerValues.map{case (tName: TestNames,tEmail: TestEmails)=> TestNameEmails(tName, tEmail)}.toSet
      assert(inner == Set(
        TestNameEmails(1,"bob","bob@bob.com"),
        TestNameEmails(1,"bob2","bob@bob.com"),
        TestNameEmails(2,"carl","carl@email.com"),
        TestNameEmails(3,"joe","joe@joe.com"),
        TestNameEmails(3,"joe","joe2@joe.com")
      ), "User 4 & 5 should not be present, since they're not in both sets.")
    }

    "Outer Join test" in {
      val outer: Set[TestNameEmails] = joined.outerValues.map{joined => joined.to(
        tName => TestNameEmails(tName.id, tName.name, "none") ,
        tEmail => TestNameEmails(tEmail.userId, "none", tEmail.email),
        (tName,tEmail) => TestNameEmails(tName, tEmail)
      )}.toSet

      assert(outer == Set(
        TestNameEmails(1,"bob","bob@bob.com"),
        TestNameEmails(1,"bob2","bob@bob.com"),
        TestNameEmails(2,"carl","carl@email.com"),
        TestNameEmails(3,"joe","joe@joe.com"),
        TestNameEmails(3,"joe","joe2@joe.com"),
        TestNameEmails(4,"sue","none"),
        TestNameEmails(5,"none","jim@jimbob.com"),
      ))
    }
  }


}
