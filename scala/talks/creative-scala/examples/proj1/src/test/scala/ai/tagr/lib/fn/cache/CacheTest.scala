package ai.tagr.lib.fn.cache

import org.scalatest.freespec.AnyFreeSpec

class CacheTest extends AnyFreeSpec{
  "Cache" - {
    "should wrap a mutable value" in {
      val get = Cache("hello")
      assert(get() == "hello")
      get.setCached("world")
      assert(get() == "world")
    }
    "should be lazy" in {
      var mutable = "hello" //lazy, not used
      val get = Cache(mutable)
      mutable = "world"
      assert(get() == "world")

      get.setCached("goodbye") // override stored
      mutable = "world2" // should do nothing
      assert(get() == "goodbye")
    }
    "should update from correct source" in {
      var mutable = "hello" //lazy, not used
      val get = Cache(mutable)
      assert(get.isEmpty)

      mutable = "world"
      assert(get() == "world", "Should retrieve a new value")

      mutable = "world2"
      assert(get() == "world", "Should use cached value")

      get.update()
      assert(get() == "world2", "Should have retrieved a new value")

      get.clear()
      assert(get.isEmpty)
      mutable = "world3"
      assert(get() == "world3", "Should retrieve a new value")

      get.setCached("world4")
      mutable = "world5" //not used yet
      assert(get() == "world4", "Should use set value")
      get.update()
      assert(get() == "world5", "Should use update value")

      var mutable2 = "aaa1"
      mutable = "world6"
      get.setNext(mutable2)
      assert(get() == "world5", "Should not update yet")

      get.update()
      assert(get() == "aaa1", "Should use new next")
      mutable2 = "aaa2"
      assert(get() == "aaa1", "Should not update yet")
      get.update()
      assert(get() == "aaa2", "Should use new next")

      var mutable3 = "bbb1"
      get.set(mutable3)
      assert(get() == "bbb1", "Should immediately update with set")
      mutable3 = "bbb2"
      get.update()
      assert(get() == "bbb2", "Should also use new next for updates")
    }
  }

}
