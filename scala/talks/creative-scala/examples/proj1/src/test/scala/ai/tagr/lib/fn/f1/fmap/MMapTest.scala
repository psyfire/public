package ai.tagr.lib.fn.f1.fmap

import org.scalatest.freespec.AnyFreeSpec

class MMapTest extends AnyFreeSpec {
  "MMap" - {
    "should return an MMap" in {
      val mmap1: MMap[Int, String] = MMap[Int, String](Map(), (i:Int) => "value: " + i, memoize = true)
        .mutate(_.view.mapValues(_ + " 2").toMap)
        .setFn(i => "vvv " + i)
        .setMemo(false)
        .+= (4 -> "test")
        .-= (4)
    }

    "should be able to replace a map" in {
      val mmap = MMap(Map(5 -> "Hello: 5"), (i: Int) => s"not found: $i")
      assert(mmap(5) == "Hello: 5")

      mmap.set(Map(5 -> "World: 5"))
      assert(mmap(5) == "World: 5")

      mmap.mutate(m => m.view.mapValues(_ + " extra").toMap)
      assert(mmap(5) == "World: 5 extra")

      mmap += 6 -> "Hi: 6"
      assert(mmap(6) == "Hi: 6")
      mmap -= 6
      assert(mmap(6) == "not found: 6")
    }

    "should be able to replace the else function" in {
      val mmap = MMap[Int, String](Map(8 -> "asdf"), (i: Int) => s"A: $i", memoize = true)
      assert(mmap(1) == "A: 1", "Should use the else function")
      assert(mmap(8) == "asdf", "Should use the stored value")
      assert(mmap() == Map(1 -> "A: 1", 8 -> "asdf"), "Should return memoized values in a map")

      mmap.setFn(i => s"B: $i")
      assert(mmap(1) == "A: 1", "Should use memoized value")
      assert(mmap(2) == "B: 2", "Should use replaced function.")

      mmap.setFn(i => s"C: $i").setMemo(false)
      assert(mmap(3) == "C: 3", "Should use replaced function.")
      mmap.setFn(i => s"D: $i")
      assert(mmap(3) == "D: 3", "Should not memoize the value, when disabled.")

      mmap.setFn(i => s"E: $i").setMemo(true)
      assert(mmap(4) == "E: 4", "Should use replaced function.")

      mmap.setFn(i => s"F: $i")
      assert(mmap(4) == "E: 4", "Should use memoized value.")

      mmap -= 4
      assert(mmap(4) == "F: 4", "Should retrieve new value from else.")

      mmap.setFn(i => s"G: $i").clear()
      assert(mmap(4) == "G: 4", "Should clear and retrieve new value from else.")

      assert(mmap() == Map(4 -> "G: 4"))
    }
  }

}
