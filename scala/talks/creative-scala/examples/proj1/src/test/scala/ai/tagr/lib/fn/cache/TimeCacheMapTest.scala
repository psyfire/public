package ai.tagr.lib.fn.cache

import org.scalatest.freespec.AnyFreeSpec

class TimeCacheMapTest extends AnyFreeSpec {
  "TimeCacheMap" - {
    "should perform most functions." in {
      val expire: Expire[Int] = (start: Int, current: Int) => current - start >= 10
      var time = 0
      val cache = new TimeCacheMap[Int, String, Int]((i: Int) => s"Hello:$i", time, expire)
      assert(cache(0) == "Hello:0")

      val fn = cache.getFn
      cache.setFn(i => s"Goodbye:$i")
      assert(cache(0) == "Hello:0", "should use cached value")
      assert(cache(1) == "Goodbye:1", "should use new function from setFn")
      assert(fn(0) == "Goodbye:0", "should use updated function")

      assert(cache() == Map(0 -> "Hello:0", 1 -> "Goodbye:1"), "should retrieve map using assert()")

      assert(cache(0) == "Hello:0")
      time = 11
      assert(cache(0) == "Goodbye:0", "Should retrieve a new value when expired")

      cache.setFn(i => s"Goodbye World:$i")
      assert(cache.force(0) == "Goodbye World:0", "force function should force a refresh")

      cache.setCached(0 -> "troll")
      assert(cache(0) == "troll", "should overwrite cached values")

      assert(!cache.hasKey(3), "hasKey")
      cache(3)
      assert(cache.hasKey(3), "hasKey")

      assert(cache.isExpired(4), "should be expired, because it's not cached yet")
      assert(cache(4) == "Goodbye World:4")
      assert(!cache.isExpired(4), "should not be expired right after update")

      time = time + 10
      cache.setFn(i => s"foo:$i")
      assert(cache.isExpired(4), "should not be expired once enough time has passed")
      assert(cache.cachedValue(4).contains("Goodbye World:4"), "shouldn't update yet")

      assert(cache(4) == "foo:4", "should update when expired, and retrieved")
      assert(!cache.isExpired(4), "...and no longer be expired")

      time = 50
      cache(5)
      assert(cache.cachedTime(5).contains(50), "should contain the time at which it was cached")
      time = 55
      cache(5)
      assert(cache.cachedTime(5).contains(50), "should contain the time at which it was cached")
      time = 61
      cache(5)
      assert(cache.cachedTime(5).contains(61), "should contain the updated time, when it was updated")

      cache.clear()
      assert(cache().isEmpty, "clear should empty the cache")
    }
  }

}
