package ai.tagr.example.creative

import org.scalatest.freespec.AnyFreeSpec

class ExFunnyTrait extends AnyFreeSpec {
  "why not a function" - {
    case class Person(id: Int, name: String, age: Int)

    "lets start with a def" in {
      def validate(p: Person): Option[String] =
        if (p.id <= 0) Some(s"Invalid Id ${p.id}")
        else if (p.name == "Bob") Some("No Bob's allowed!")
        else None

      assert(validate(Person(12, "Bob", 16)).contains("No Bob's allowed!"))
      assert(validate(Person(-1, "Sarah", 20)).contains("Invalid Id -1"))
      assert(validate(Person(5, "Joe", 30)).isEmpty)
      assert(validate(Person(-12, "Bob", 18)).contains("Invalid Id -12"))
      // Technically there are two validation errors, but at least we stopped Bob at the invalid id step
    }



    "trait extends a function?" in {
      trait ValidatePerson extends (Person=>Option[String])

      // can construct with function syntax
      val validate: ValidatePerson = (p: Person) => {
        if (p.id <= 0) Some(s"Invalid Id ${p.id}")
        else if (p.name == "Bob") Some("No Bob's allowed!")
        else None
      }

      assert(validate(Person(-12, "Bob", 18)).contains("Invalid Id -12"))

      // ok, this isn't very interesting yet.
      // but wait there's more!
    }

    "ok, this is getting interesting" in {
      trait ValidatePerson extends (Person=>Iterable[String]) {
        def apply(person: Person): Iterable[String]

        def ++ (other: ValidatePerson): ValidatePerson =
          (p: Person) => this(p) ++ other(p)
      }

      val validateId: ValidatePerson = (p: Person)=>
        if (p.id <= 0) Some(s"Invalid Id ${p.id}")
        else None

      val inValidateBob: ValidatePerson =  (p: Person)=>
        if (p.name == "Bob") Some("No Bob's allowed!")
        else None

      val validate = validateId ++ inValidateBob

      val res = validate(Person(-12, "Bob", 18)).toSet
      assert(res.contains("Invalid Id -12"))
      assert(res.contains("No Bob's allowed!"))
    }

  }
}
