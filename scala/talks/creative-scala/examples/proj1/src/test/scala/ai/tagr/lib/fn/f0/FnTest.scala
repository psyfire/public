package ai.tagr.lib.fn.f0

import ai.tagr.lib.fn.cache.Cache
import org.scalatest.freespec.AnyFreeSpec


class FnTest extends AnyFreeSpec {
  "Fn" - {
    "should get a value" in {
      val get: Fn[String] = "hello"
      assert(get() == "hello")
    }

    "should get a value later" in {
      var mutable = "hello"
      val get: Fn[String] = mutable
      mutable = "world"
      assert(get() == "world")
    }
  }

  "Lazy" - {
    "should be lazy" in {
      var mutable = "hello"
//      val get: F0[String] = F.Lazy(mutable)
      val get: Fn[String] = Fn(mutable).toLazy
      mutable = "world"
      assert(get() == "world")
      mutable = "nope"
      assert(get() == "world")
    }
  }

  "Val should behave like a val" in {
    var mutable = "value1"
    val get = Fn.Val(mutable)
    mutable = "value2" // should be ignored
    assert(get() == "value1")
  }

  "FnLike" -{
    "map" in {
      val mutable = Fn.Var("value1")
      val mapped = mutable.map(_ + ":1")
      assert(mapped() == "value1:1")
      mutable.set("value2")
      assert(mapped() == "value2:1")
    }

    "is" in {
      val get = Fn.Var(10)
      assert(get.is(10))
      assert(!get.is(11))

      get.set(11)
      assert(!get.is(10))
      assert(get.is(11))
    }

    "Cache + map" in {
      var mutable = 1
      val cache = Cache(mutable)
      val mapped = cache.map("hello:" + _)
      assert(mapped.is("hello:1"))

      cache.setCached(2)
      assert(mapped.is("hello:2"))

      cache.update()
      assert(mapped.is("hello:1"))

      mutable = 3
      assert(mapped.is("hello:1"))
      cache.update()
      assert(mapped.is("hello:3"))

      var mutable2 = 4
      cache.set(mutable2)
      mutable2 = 5
      assert(mapped.is("hello:4"))
      cache.update()
      assert(mapped.is("hello:5"))
    }

    "toSome" in {
      val get = Fn(5)
      val mutable = get.toVar
      val opt = mutable.toSome

      assert(opt.contains(5))

      mutable.set(10)
      assert(opt.contains(10))
    }

    "collectFirst" in {
      val get = Fn(5)
      val res1 = get.collectFirst{case i: Int if i <= 10 => "Hello: " + i}
      val res2 = get.collectFirst{case i: Int if i > 10 => "Hello: " + i}

      assert(res1.contains("Hello: 5"))
      assert(res2.isEmpty)
    }
  }

}
