package ai.tagr.lib.fn.f1.fmap

import org.scalatest.freespec.AnyFreeSpec

class FMapTest extends AnyFreeSpec{
  "FMap" - {
    "should do basic stuff" in {
      val innerMap = Map(
        3 -> "value:3",
        4 -> "value:4"
      )

      val map: FMap[Int, String] = FMap(innerMap, (i:Int) => "missing:"+i)

      assert(map() == innerMap)
      assert(map(3) == "value:3")
      assert(map(4) == "value:4")
      assert(map(5) == "missing:5")

      val map2 = map.map("Hello "+_)
      assert(map2(3) == "Hello value:3")
      assert(map2(6) == "Hello missing:6")

      val map3 = map.mapInput((i:String) => i.toInt)
      assert(map3("4") == "value:4")
      assert(map3("5") == "missing:5")
    }


  }

}
