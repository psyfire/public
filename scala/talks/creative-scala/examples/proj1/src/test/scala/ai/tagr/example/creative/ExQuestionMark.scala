package ai.tagr.example.creative

import org.scalatest.freespec.AnyFreeSpec

case class SuperAwesome(
  thing: String
) {
  def unfinished(input: Double): Int = ???
}

class Ex1QuestionMark extends AnyFreeSpec {
  "Thing Test" - {
    "should do things" in {
      /*
      This compiles, but if we run this, we'll get an exception as expected.:

      an implementation is missing
      scala.NotImplementedError: an implementation is missing
        at scala.Predef$.$qmark$qmark$qmark(Predef.scala:347)
        at ai.tagr.example.creative.SuperAwesome.unfinished(Ex1QuestionMark.scala:8)
        at ai.tagr.example.creative.Ex1QuestionMark.$anonfun$new$2(Ex1QuestionMark.scala:17)
       */
      val awesomeA = SuperAwesome("aaay!")
      val awesomeB = SuperAwesome("to bee, or not to bzzzzz")

      val res1 = awesomeA.unfinished(3.1415)
      val res2 = awesomeB.unfinished(9001)

      val totalAwesome: Int = res1 + res2
    }
  }
}