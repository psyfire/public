package ai.tagr.lib.fn.f1.fmap

import ai.tagr.lib.fn.f1.Fn1Var
import org.scalatest.freespec.AnyFreeSpec

class MemoizeTest extends AnyFreeSpec {
  "Memoize" - {
    "Should memoize values" in {
      var mutable = "can't touch this"
      val fn = (i: Int) => i match {
        case _ if (i < 0) => s"Goodbye:${-i}"
        case _ if (i <= 10) => s"$mutable:$i"
        case _ if (i >= 9000) => s"Over 9000:$i"
        case _ => s"Hello:$i"
      }
      val memo = Memoize(fn)
      assert(memo().isEmpty)

      assert(memo(-5) == "Goodbye:5")
      assert(memo(10) == "can't touch this:10")
      assert(memo(11) == "Hello:11")
      assert(memo(9001) == "Over 9000:9001")

      mutable = "weak"
      assert(memo(9) == "weak:9")
      assert(memo(10) == "can't touch this:10")

      assert(memo() == Map(
        -5   -> "Goodbye:5",
        9    -> "weak:9",
        10   -> "can't touch this:10",
        11   -> "Hello:11",
        9001 -> "Over 9000:9001"
      ))

      assert(memo(10) == "can't touch this:10")
      memo.clear()
      assert(memo().isEmpty)
      assert(memo(10) == "weak:10")
      assert(memo().size == 1)
    }

    "Should memoize another Fn1 Function" in {
      val baseFn = Fn1Var((i: Int) => s"Hello:$i")
      val memo = baseFn.memoize()

      assert(memo(1) == "Hello:1")

      baseFn.set(i => s"World:$i")
      assert(memo(2) == "World:2")
      assert(memo(1) == "Hello:1")

      memo.clear()
      assert(memo(1) == "World:1")
    }
  }

}
