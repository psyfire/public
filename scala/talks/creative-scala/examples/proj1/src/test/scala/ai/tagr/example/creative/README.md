## ExQuestionMark

[ExQuestionMark.scala](ExQuestionMark.scala)

Shows how using `???` is great for stubbing methods, and building prototypes.

## ExFunClass

[ExFunClass.scala](ExFunClass.scala)

Shows the advantage of replacing methods and functions with classes

## ExFunnyTrait

[ExFunnyTrait.scala](ExFunnyTrait.scala)

Shows advantages of a trait, that extends a function.

## ExNested

[ExNested.scala](ExNested.scala) with [ExCache.scala](ExCache.scala)

Shows how one can use nested `object`s and `case classes` to achieve some interesting results.

* Why extending function is useful
* What looks like a method is actually a case-class, or object with an `apply` method
* Using a sub-case-class, we can further customize the output.
* Note: Ex3Cache is just for demonstration purposes & now how I'd actually design a cache.