package ai.tagr.lib.fn.f1

import ai.tagr.lib.fn.f0.Fn
import org.scalatest.freespec.AnyFreeSpec

class Fn1Test extends AnyFreeSpec {
  "Fn1" - {
    "should act like a function" in {
      // constructed with function syntax
      val fn: Fn1[Int, String] = (i: Int) => s"Hello:$i"
      assert(Some(10).map(fn).contains("Hello:10"), "should be usable as a function")
      assert(fn(11) == "Hello:11", "should support apply method")
    }

    "should support mapping input" in {
      val fn: Fn1[Int, String] = (i: Int) => s"Hello:$i"
      val fn2 = fn.mapInput((s: String) => s.toInt)
      assert(fn2("10") == "Hello:10")
      assert(fn2("11") == "Hello:11")
    }

    "should support mapping output" in {
      val fn: Fn1[Int, String] = (i: Int) => s"Hello:$i"
      val fn2 = fn.map(s => s"$s suffix")
      assert(fn2(10) == "Hello:10 suffix")
      assert(fn2(11) == "Hello:11 suffix")
    }

    "should use `f()` function" in {
      val fn: Fn1[Int, String] = (i: Int) => s"Hello:$i"
      val f: Fn[String] = fn.fn(10)
      assert(f() == "Hello:10")
    }
  }

}
