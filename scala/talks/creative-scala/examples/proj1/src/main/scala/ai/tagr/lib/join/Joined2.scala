package ai.tagr.lib.join

/** Similar to `Either` except both can be defined. Useful for joining two different types that may exist, into a single value.
 * Use `Join` to create instances of `Joined2` from two sets, based on a common field/key/value. */
case class Joined2[A,B](a: Option[A], b: Option[B]) {
  if (a.isEmpty && b.isEmpty) throw new IllegalArgumentException("Must define one or both of 'a' or 'b'.")

  lazy val hasBoth: Boolean = a.isDefined && b.isDefined
  lazy val both: Option[(A, B)] = if (hasBoth) Some((a.get, b.get)) else None
  lazy val getBoth: (A, B) = both.get
  lazy val tuple: (Option[A], Option[B]) = (a,b)


  /** Convert all pairs to a single type.  When both are present, fnA is used.
   * @param fnA Run this, when only `a` is present.
   * @param fnB Run this, when only `b` is present.
   */
  def to[C](fnA: A=>C,fnB: B=>C): C =
    to(fnA, fnB, (a,_) => fnA(a))

  /** Convert all pairs to a single type.
   * @param fnA Run this, when only `a` is present.
   * @param fnB Run this, when only `b` is present.
   * @param fnAB Run this, when both `a` and `b` are present.
   */
  def to[C](fnA: A=>C,fnB: B=>C, fnAB: (A,B)=>C): C = {
    this match {
      case Joined2(Some(ai),Some(bi)) => fnAB(ai,bi)
      case Joined2(Some(ai),None) => fnA(ai)
      case Joined2(None,Some(bi)) => fnB(bi)
      case _ => throw new IllegalArgumentException("impossible")
    }
  }

  /** Transform both `a` and `b` values. */
  def map[C,D](fnA: A=>C,fnB: B=>D): Joined2[C,D] = Joined2(a.map(fnA),b.map(fnB))

  /** Transform the `a` values only. */
  def mapA[C](fn: A=>C): Joined2[C,B] = Joined2(a.map(fn),b)
  /** Transform the `b` values only. */
  def mapB[C](fn: B=>C): Joined2[A,C] = Joined2(a,b.map(fn))

  /** Returns a Seq with all present elements, returning one or two values.  If both present, the first element will be `a` and second `b` */
  def toSeq[C](fnA: A=>C,fnB: B=>C): Seq[C] = {
    val j2 = map(fnA, fnB)
    Seq(j2.a, j2.b).flatten
  }
}

object Joined2{
  def a[A,B](a: A): Joined2[A, B] = Joined2(Some(a), None)
  def b[A,B](b: B): Joined2[A, B] = Joined2(None, Some(b))
  def ab[A,B](a: A, b:B): Joined2[A, B] = Joined2(Some(a), Some(b))
}


/** A simplified version of joined, where all elements have the same type. */
case class Joined2A[A](a: Option[A], b: Option[A]) {
  val joined = Joined2(a,b)

  lazy val hasBoth: Boolean = joined.hasBoth
  lazy val both: Option[(A, A)] = joined.both
  lazy val getBoth: (A, A) = joined.getBoth

  def to[C](fn: A=>C, fnAB: (A,A)=>C): C = {
    this match {
      case Joined2A(Some(ai),Some(bi)) => fnAB(ai,bi)
      case Joined2A(Some(ai),None) => fn(ai)
      case Joined2A(None,Some(bi)) => fn(bi)
      case _ => throw new IllegalArgumentException("impossible")
    }
  }

  def map[C](fn: A=>C): Joined2[C,C] = joined.map(fn, fn)
  def map[C](fnA: A=>C, fnB: A=>C): Joined2[C,C] = joined.map(fnA, fnB)

  /** Returns a Seq with one or two elements.  If both present, the first element will be `a` and second `b` */
  lazy val toSeq: Seq[A] = Seq(a,b).flatten
}