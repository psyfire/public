package ai.tagr.lib.fn.f0

/** Option implementation of Fn.
  * Acts like a `def {name}: Option[A] = ...` */
trait Opt[A]
  extends (() => Option[A])
    with OptLike[A]
    with Fn[Option[A]]


object Opt{
  def apply[A](a: => Option[A]): Opt[A] = () => a

  def some[A](a:Fn[A]): Opt[A] = Opt(Some(a.apply()))
  def none[A]: Opt[A] = apply(None)

  /** Immutable fixed-value implementation of Fn.
    * Acts like a `val {name}: Option[A] = ...` */
  def Val[A](a: Option[A]): Opt[A] = () => a

  /** Immutable-lazy-value implementation of Fn.
    * Acts like a `lazy val {name}: A = ...` */
  def Lazy[A](initial: => Option[A]): Opt[A] = Opt{
    lazy val _lazy = initial
    Opt(_lazy)
  }

  def Var[A](a: Option[A]): MOpt[A] = MOpt(a)

  implicit def from[A](a: => Option[A]):Opt[A] = () => a
  implicit def get[A](get: Opt[A]): Option[A] = get.apply()
}

/** MOpt or "Mutable Option"
  * Mutable Option implementation of Fn.
  * Acts like a `var {name}: Option[A] = ...` */
class MOpt[A](val a: Var[Option[A]]) extends Opt[A] with VarLike[Option[A], MOpt[A]]{
  override def initialValue: Option[A] = a
  override def self: MOpt[A] = this
  def innerMutate(f: A => A): MOpt[A] = mutate(_.map(f)) // TODO better name?
}

object MOpt {
  def apply[A](a: => Option[A]): MOpt[A] = new MOpt[A](Var(a))
}