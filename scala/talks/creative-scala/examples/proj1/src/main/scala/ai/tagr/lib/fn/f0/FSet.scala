package ai.tagr.lib.fn.f0

trait FSet[A] extends (() => TraversableOnce[A]) with FSetLike[A]

object FSet {
  def apply[A](set: => TraversableOnce[A]): FSet[A] = () => set
  def <== [A](elems: A*): FSet[A] = apply(elems)

  val Var: MSet.type = MSet
  type Var[A] = MSet[A]

  val Val: FSetVal.type = FSetVal
  type Val[A] = FSetVal[A]

  val Lazy: FSetLazy.type = FSetLazy
  type Lazy[A] = FSetLazy[A]
}

/** Mutable Function Set */
class MSet[A](initial: => TraversableOnce[A]) extends FSet[A] with VarLike[TraversableOnce[A], MSet[A]]{
  override def initialValue: TraversableOnce[A] = initial
  override def self: MSet[A] = this
}

object MSet {
  def apply[A](set: => TraversableOnce[A]): MSet[A] = new MSet(set)
}


class FSetVal[A](set: TraversableOnce[A]) extends FSet[A]{
  override def apply(): TraversableOnce[A] = set
}

object FSetVal {
  def apply[A](set: TraversableOnce[A]): MSet[A] = new MSet(set)
}

class FSetLazy[A](get: FSet[A]) extends FSet[A] {
  private lazy val _lazy:TraversableOnce[A] = get()
  override def apply(): TraversableOnce[A] = _lazy
}

object FSetLazy{
  def apply[A](retrieve: => TraversableOnce[A]): FSetLazy[A] = new FSetLazy[A](() => retrieve)
}

trait FSetLike[A] {
  def apply(): TraversableOnce[A]

  /** ----------------------------- Map, FlatMap, Filter, etc  -----------------------------  */
  def map[B](f: A=>B): FSet[B] = FSet(apply().map(f))

  def flatMap[B](f: A=>FSet[B]): FSet[B] = FSet(apply().flatMap(f(_).apply()))

  def filter(p: A => Boolean): FSet[A] = FSet(apply().filter(p))

  // TODO: Definitely test this.
  def collect[B](pf: PartialFunction[A, B]): FSet[B] = FSet(apply().flatMap(a => pf.lift(a)))

  def contains[A1 >: A](elem: A1*): Boolean = containsAll(elem)
  def containsAll[A1 >: A](elem: TraversableOnce[A1]): Boolean = apply().exists(e => elem.exists(e2 => e2 == e))

  def exists(p: A => Boolean): Boolean = apply().exists(p)
  def forall(p: A => Boolean): Boolean = apply().forall(p)

  def foreach[U](f: A => U): Unit = apply().foreach(f)
}