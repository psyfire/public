package ai.tagr.lib.fn.f0

import ai.tagr.lib.fn.f0

/** Represents the ability to retrieve a value. */
trait Fn[A] extends (() => A) with FnLike[A]

object Fn{
  /** Function implementation of Fn.
    * Acts like a `def {name}: A = ...` with no parameters */
  def apply[A](a: => A):Fn[A] = () => a

  /** Immutable fixed-value implementation of Fn.
    * Acts like a `val {name}: A = ...` */
  def Val[A](initial: => A): Fn[A] = Fn(initial).toVal

  /** Mutable-value implementation of Fn.
    * Acts like a `var {name}: A = ...` */
  import ai.tagr.lib.fn.f0.{Var => FnVar}
  val Var: FnVar.type = FnVar
  type Var[A] = FnVar[A]

  /** Immutable-lazy-value implementation of Fn.
    * Acts like a `lazy val {name}: A = ...` */
  def Lazy[A](initial: => A): Fn[A] = Fn(initial).toLazy

  /** Collection implementation of Fn.
    * Acts like a `def {name}: Set[A] = ...` */
  val Set: FSet.type = FSet
  type Set[A] = FSet[A]

  /** Option implementation of Fn.
    * Acts like a `def {name}: Option[A] = ...`
    * Since `Opt` can be accessed directly, it's usually better to use `Opt` instead of `Fn.Opt`*/
  import ai.tagr.lib.fn.f0.{Opt => FnOpt}
  val Opt: f0.Opt.type = FnOpt
  type Opt[A] = FnOpt[A]

  implicit def from[A](a: => A):Fn[A] = apply(a)
  implicit def get[A](get: Fn[A]):A = get.apply()
}

