package ai.tagr.lib.fn.f0

import ai.tagr.lib.fn.Self


trait VarLike[A, SELF <: VarLike[A, SELF]] extends Fn[A] with Self[SELF] {
  /** Used to construct the var as an initial value.
    * Should not be used except by the Var trait. */
  def initialValue: A

  private var _value = initialValue

  override def apply(): A = _value

  /**
    * Replace the value contained.
    */
  def set(a: A): SELF = {
    _value = a
    self
  }

  /** Mutate a Var with a function.  Operates similarly to `map(f: A=>B)` except sets the inner value, instead of returning a new monad */
  def mutate(f: A => A): SELF = set(f(apply()))
}

