package ai.tagr.lib.fn.f1.fmap

import ai.tagr.lib.fn.f0.Fn
import ai.tagr.lib.fn.f1.Fn1Opt

/**
  * A function backed by a map, which returns an optional value if present, or none if not present.
  * */
trait OMap[K,V] extends Fn1Opt[K,V] {
  def fnMap: Fn[Map[K,V]]
  override def apply(key: K): Option[V] = fnMap.get(key)
}


object OMap {
  def apply[K,V](m: => Map[K, V]): OMap[K,V] = new OMap[K,V] {
    override def fnMap: Fn[Map[K, V]] = m
  }

  def Val[K,V](m: Map[K, V]): OMap[K,V] = new OMap[K,V] {
    override def fnMap: Fn[Map[K, V]] = m
  }

  def Lazy[K,V](m: => Map[K, V]): OMap[K,V] = new OMap[K,V] {
    private lazy val _lazy = m
    override def fnMap: Fn[Map[K, V]] = _lazy
  }
}
