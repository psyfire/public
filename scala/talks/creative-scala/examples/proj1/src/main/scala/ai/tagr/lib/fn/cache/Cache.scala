package ai.tagr.lib.fn.cache

import ai.tagr.lib.fn.f0.Fn

trait CacheT[A] extends Fn[A] {
  def set(t:Fn[A]): CacheT[A]
  def setNext(t:Fn[A]): CacheT[A]
  def setCached(t:A): CacheT[A]

  def isEmpty: Boolean
  def cached: Option[A]

  def update(): CacheT[A]
  def clear(): CacheT[A]
}


/** Lazy Mutable Cache of a value.
  * Can act like a lazy mutable var by using `set()` to explicitly overwrite the value.
  * Can act like a cache, retrieving only on request, or when `update` is called to force an update. */
class Cache[A](next: Fn[A]) extends CacheT[A] {
  /** The function used to retrieve the next value on update. */
  private var _next:Fn[A] = next
  private var _value: Option[A] = None

  override def apply(): A = {
    if (_value.isEmpty) {
      _value = Some(_next())
    }
    _value.get
  }

  override def set(t: Fn[A]): Cache[A] = {
    setNext(t)
    update()
  }

  /** Replaces the value retriever.  Does not update cached value.   */
  override def setNext(next: Fn[A]): Cache[A] = {
    _next = next
    this
  }

  /** Overwrites the cached value.
    * This value will be overwritten on the next update. */
  override def setCached(t:A): Cache[A] = {
    _value = Some(t)
    this
  }

  override def isEmpty: Boolean = _value.isEmpty
  override def cached: Option[A] = _value

  override def update(): Cache[A] = {
    setCached(_next())
    this
  }

  override def clear(): Cache[A] = {
    _value = None
    this
  }


}

object Cache{
  def apply[A](next: Fn[A]): Cache[A] = new Cache[A](next)
}
