package ai.tagr.lib.fn.f1

/** A function that returns an Option. */
// TODO: This could potentially use val/mutable/lazy/etc variants.
trait Fn1Opt[IN,A] extends (IN => Option[A]) with Fn1[IN,Option[A]]{

  def getOrElse(in: IN, default: => A): A = apply(in).getOrElse(default)
  /** Provide a default for when the function returns None.
    * Allows conversion from an F1.Opt to a F1*/
  def orElse(f: Fn1[IN, A]) :Fn1[IN,A] = Fn1((in:IN) => apply(in).getOrElse(f(in)))
}

object Fn1Opt {
  def apply[IN,A](f: IN => Option[A]):Fn1Opt[IN,A] = (p: IN) => f(p)
}
