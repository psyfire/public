package ai.tagr.lib.fn.cache

import ai.tagr.lib.fn.f0.Fn

/**
  * A cache that expires based on the passage of time.
  *
  * @param getFn A function that can produce the desired value.
  * @param timeFn A function that returns the current time.
  * @param expire A function that determines if a cache should be considered expired based on the passage of time.
  * @tparam VALUE The type of the cached value
  * @tparam TIME The type of time, to be used for expiration of the cache.
  */
class TimeCache[VALUE,TIME](getFn: Fn[VALUE], timeFn: Fn[TIME], expire: Expire[TIME]) extends CacheT[VALUE]{
  private val _cachedValue: Cache[VALUE] = Cache(getFn)
  private val _cachedTime: Cache[TIME] = Cache(timeFn)

  override def apply(): VALUE = {
    if (isExpired) {
      update()
    }
    _cachedValue()
  }


  override def set(t: Fn[VALUE]): TimeCache[VALUE,TIME] = {
    setNext(t)
    update()
  }

  override def setNext(t: Fn[VALUE]): TimeCache[VALUE,TIME] = {
    _cachedValue.setNext(t)
    this
  }

  override def setCached(t: VALUE): TimeCache[VALUE,TIME] = {
    _cachedValue.setCached(t)
    _cachedTime.update()
    this
  }

  override def update(): TimeCache[VALUE,TIME] = {
    _cachedValue.update()
    _cachedTime.update()
    this
  }

  override def isEmpty: Boolean = _cachedValue.isEmpty || _cachedTime.isEmpty

  def isExpired: Boolean = isEmpty || expire(_cachedTime(), timeFn())

  override def clear(): TimeCache[VALUE,TIME] = {
    _cachedValue.clear()
    _cachedTime.clear()
    this
  }

  override def cached: Option[VALUE] = _cachedValue.cached
  def cachedTime: Option[TIME] = _cachedTime.cached
  /** Retrieves the last updated time, or forced an update if nothing is cached. Avoids option, but may force an early update.*/
  def getCachedTime: TIME = cachedTime.getOrElse(update().cachedTime.get)
}

object TimeCache{
  def apply[VALUE,TIME](getFn: Fn[VALUE], timeFn: Fn[TIME], expire: Expire[TIME]): TimeCache[VALUE,TIME] = new TimeCache(getFn, timeFn, expire)
}