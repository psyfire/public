package ai.tagr.lib.fn.f0

/** Mimics functionality of Scala's `Option`
  * Operations that return an Opt[T] or Get[T] are lazy */
trait OptLike[A] extends FnLike [Option[A]] {self =>
  /** Override to enable functionality. */
  def apply(): Option[A]

  /** ------------------------------- Convert ------------------------------- */
  def toF0: Fn[Option[A]] = Fn(apply())
  def toSet: Fn.Set[A] = Fn.Set(apply())
  /** ------------------------------- Option ------------------------------- */

  def isEmpty: Boolean = apply().isEmpty

  def isDefined: Boolean = !isEmpty

  def get: Fn[A] = Fn(apply().get)

  def getOrElse[B >: A](default: => B): Fn[B] = Fn[B](apply().getOrElse(default))

  def orNull[A1 >: A](implicit ev: Null <:< A1): Fn[A1] = Fn(apply().orNull)

  def map[B](f: A => B): Opt[B] = Opt(apply().map(f))

  def fold[B](ifEmpty: => B)(f: A => B): Fn[B] = Fn(apply().fold(ifEmpty)(f))

  def flatMap[B](f: A => Option[B]): Opt[B] = Opt(apply().flatMap(f))

  def flatten[B](implicit ev: A <:< Option[B]): Opt[B] = Opt(apply().flatten)

  def filter(p: A => Boolean): Opt[A] = Opt(apply().filter(p))

  def filterNot(p: A => Boolean): Opt[A] = Opt(apply().filterNot(p))

  def nonEmpty: Boolean = isDefined

  /** Necessary to keep $option from being implicitly converted to
    *  [[scala.collection.Iterable]] in `for` comprehensions. */
  def withFilter(p: A => Boolean): WithFilter = new WithFilter(p)
  class WithFilter(p: A => Boolean) {
    def map[B](f: A => B): Opt[B] = self filter p map f
    def flatMap[B](f: A => Option[B]): Opt[B] = self filter p flatMap f
    def foreach[U](f: A => U): Unit = self filter p foreach f
    def withFilter(q: A => Boolean): WithFilter = new WithFilter(x => p(x) && q(x))
  }

  def contains[A1 >: A](elem: A1): Boolean = apply().contains(elem)

  def exists(p: A => Boolean): Boolean = apply().exists(p)

  def forall(p: A => Boolean): Boolean = apply().forall(p)

  def foreach[U](f: A => U): Unit = apply().foreach(f)

  def collect[B](pf: PartialFunction[A, B]): Opt[B] = Opt(apply().collect(pf))

  def orElse[B >: A](alternative: => Option[B]): Opt[B] = Opt(apply().orElse(alternative))

  def iterator: Iterator[A] = apply().iterator

  // TODO: maybe reimplement this if a Get List is created
  def toList: Fn[List[A]] = Fn(apply().toList)

  // TODO: maybe reimplement this if a Get Either is created
  def toRight[X](left: => X): Fn[Either[X, A]] = apply().toRight(left)

  // TODO: maybe reimplement this if a Get Either is created
  def toLeft[X](right: => X): Fn[Either[A, X]] = apply().toLeft(right)

}
