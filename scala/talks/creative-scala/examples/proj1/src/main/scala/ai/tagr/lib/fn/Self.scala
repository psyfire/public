package ai.tagr.lib.fn

trait Self[S <: Self[S]] {
  type SelfType = S
  def self: S
}