package ai.tagr.lib.fn.f1

import ai.tagr.lib.fn.f0.{Fn, VarLike}
import ai.tagr.lib.fn.f1.fmap.{FMap, Memoize}


/**
  * Represents a function with 1 parameter
  *
  * @tparam IN Input parameter type
  * @tparam A Return type
  */
trait Fn1[IN, A] extends (IN => A) with Fn1Like[IN,A]


object Fn1{
  def apply[IN, A](f: IN => A):Fn1[IN,A] = (in:IN) => f(in)
  def of[IN, A](f: IN => Fn[A]):Fn1[IN,A] = (in:IN) => f(in)


  def from[IN,A](get: Fn[A]): Fn1[IN, A] = _ => get()

  val Opt: Fn1Opt.type = Fn1Opt
  type Opt[IN,A] = Fn1Opt[IN,A]

  val Map: FMap.type = FMap
  type Map[K,V] = FMap[K,V]

  var Var = Fn1Var
  type Var[IN,A] = Fn1Var[IN,A]
}

/**
  * TODO better name?
  *    MFn1
  *    VarFn1
  *    DynamicFn
  *    MutableFn
  *    MFunct
  *    MDef1
  */
class Fn1Var[IN,A](f: IN => A) extends Fn1[IN,A] with VarLike[(IN => A), Fn1Var[IN,A]]  {
  override def initialValue: IN => A = f
  override def self: Fn1Var[IN, A] = this

  override def apply(in: IN): A = apply().apply(in)
}

object Fn1Var {
  def apply[IN, A](f: IN => A): Fn1.Var[IN,A] = new Fn1.Var[IN,A](f)
}

trait Fn1Like[IN,A]{
  def apply(in: IN): A

  /** Retrieves the value, but wraps it in an F0.  Useful for deferred execution. */
  def fn(in:IN): Fn[A] = Fn(apply(in))

  /** Transforms the input parameter. */
  def mapInput[IN2](f: IN2 => IN): Fn1[IN2,A] = in => apply(f(in))

  /** Transform the result of an F1 */
  def map[B](f: A => B): Fn1[IN,B] = (in:IN) => f(apply(in))

  def memoize(): Memoize[IN,A] = Memoize(fn)
}