package ai.tagr.lib.fn.f1.fmap

import ai.tagr.lib.fn.f1.Fn1

/**
  * Memoize the values returned by a function.  Can be used to increase performance by reducing recalculation of values.
  *
  * @param fn The function to memoize.
  * @tparam IN The input type.
  * @tparam A The output type.
  */
class Memoize[IN,A](fn: IN => A) extends FMap[IN,A] {
  private val _memoize = MMap[IN,A](map = Map(), fn = fn, memoize = true)
  override def getFn: Fn1[IN, A] = _memoize.getFn

  override def apply(): Map[IN, A] = _memoize.apply()
  override def apply(in: IN): A = _memoize(in)

  /** Reset memoized values. */
  def clear(): Memoize[IN, A] = {
    _memoize.clear()
    this
  }
}

object Memoize {
  def apply[IN,A](fn: IN => A): Memoize[IN, A] = new Memoize[IN,A](fn)
}