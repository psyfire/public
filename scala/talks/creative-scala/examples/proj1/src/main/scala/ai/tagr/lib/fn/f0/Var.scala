package ai.tagr.lib.fn.f0

// TODO better name for Var?
// mutable - maybe
// dynamic -no
// set - no
// morph
// var
// flexible - no
// Fluid

/** Mutable-value implementation of Fn.
  * Acts like a `var {name}: A = ...` */
class Var[A](initial: => A) extends VarLike[A,Var[A]] {
  override def initialValue: A = initial
  override def self: Var[A] = this
}

object Var{
  def apply[A](a: A): Var[A] = new Var(a)
}
