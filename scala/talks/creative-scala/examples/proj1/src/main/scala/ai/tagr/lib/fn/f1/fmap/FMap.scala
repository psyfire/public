package ai.tagr.lib.fn.f1.fmap

import ai.tagr.lib.fn.f1.Fn1

/** `FMap` or "Function Map"
  * Use a map like a Function, where the key is the input and the value is the output.  To handle cases with a missing key
  * an `getFn` "get Function" is provided.  This allows the function to return a type `V` instead of an `Option[V]`
  *
  * Also see:
  *   `OMap` or "Option Map" - A function backed by a map, which returns an Optional value.
  *   `MMap` or "Mutable Nap" - A FMap which allows mutating the map or default function, such as adding values.
  *   `Memoize` - Memoize the values returned by a function.  Can be used to increase performance by reducing recalculation of values.
  *   */
trait FMap[K,V] extends Fn1[K,V]  { //with Fn[Map[K,V]]
  /** "Get Function" A default function to use, when the base-map doesn't contain the requested value. */
  def getFn: Fn1[K,V]

  /** The underlying map from which values are retrieved. */
  def apply(): Map[K,V]

  /** Retrieve the requested key from the map, or from the `getFn` when not present.*/
  override def apply(key: K): V = apply().getOrElse(key, getFn(key))
}


object FMap {
  def apply[K,V](m: => Map[K, V], orElse: Fn1[K,V]): FMap[K,V] = new FMap[K,V] {
    override def apply(): Map[K, V] = m
    override def getFn: Fn1[K, V] = orElse
  }

  def apply[K,V](m: => Map[K, V]): OMap[K,V] = OMap(m)

  def Val[K,V](m: Map[K, V], orElse: K => V): FMap[K,V] = new FMap[K,V] {
    override def apply(): Map[K, V] = m
    override def getFn: Fn1[K, V] = k => orElse(k)
  }

  def Val[K,V](m: Map[K, V]): OMap[K,V] = OMap.Val(m)

  def Var[K,V](map: Map[K,V], orElse: K => V, memoize: Boolean = false): Var[K, V] = new MMap[K,V](map, orElse, memoize)
  type Var[K,V] = MMap[K,V]

  def Lazy[K,V](m: => Map[K, V], orElse: Fn1[K,V]): FMap[K,V] = new FMap[K,V] {
    private lazy val _lazy = m
    override def apply(): Map[K, V] = _lazy
    override def getFn: Fn1[K, V] = orElse
  }


  def Lazy[K,V](m: => Map[K, V]): OMap[K,V] = OMap.Lazy(m)
}





