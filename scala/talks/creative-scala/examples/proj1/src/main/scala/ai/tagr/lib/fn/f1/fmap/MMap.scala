package ai.tagr.lib.fn.f1.fmap

import ai.tagr.lib.fn.f0.VarLike
import ai.tagr.lib.fn.f1.Fn1

/** "MMap" or "Mutable Map"
  * A fully mutable FMap, which supports mutating the backing map, default function, and memoizing values.
  *
  * @param map The initial map or data backing this.
  * @param fn If the map is missing a value, use the output of this function instead.
  * @param memo If true, memoize the value returned by the `orElse` function, storing it in the map.
  * @tparam K The type of the key used to retrieve values.
  * @tparam V The type of the value returned by the map.
  */
class MMap[K,V](
  map: Map[K,V],
  fn: K => V,
  var memo: Boolean = false
) extends FMap[K,V]
  with VarLike[Map[K,V], MMap[K,V]]
{
  override def initialValue: Map[K, V] = map
  override def self: MMap[K, V] = this

  private val _fn = Fn1.Var(fn)
  override def getFn: Fn1.Var[K, V] = _fn

  def setFn(fn: K => V): MMap[K, V] = {
    getFn.set(fn)
    this
  }

  def +=(kv: (K,V)): MMap[K, V] =
    mutate(map => map + kv)


  def -=(key: K): MMap[K, V] =
    mutate(map => map - key)


  def setMemo(memo: Boolean): MMap[K, V] = {
    this.memo = memo
    this
  }

  /**
    * Retrieve the value for the given key from the map if it exists, or use the `fn` function as a fallback.
    */
  override def apply(key: K): V = {
    val found = apply().get(key)
    found match {
      case Some(v) => v
      case _ => force(key, memo)
    }
  }

  /** Ignores the map, and retrieves the value directly from the function.
    * If memoize is enabled, the value in the map will be updated. */
  def force(key: K, memoize: Boolean = memo): V = {
    val v = getFn(key)
    if (memoize) {
      this += key -> v
    }
    v
  }

  def clear(): MMap[K, V] = set(Map())
}

object MMap {
  def apply[K,V](
    map: Map[K,V] = Map(),
    fn: K => V = exceptionElse,
    memoize: Boolean = false
  ): MMap[K,V] = new MMap[K,V](map, fn, memoize)

  def exceptionElse[K,V]: K=>V = (k:K) => throw new IllegalArgumentException(s"Map does not contain '$k', and default function not specified for MMap.")
}