package ai.tagr.lib.fn.cache

trait Expire[TIME] extends ((TIME,TIME)=>Boolean) {
  override def apply(last: TIME, current: TIME): Boolean
  def &&(other: Expire[TIME]): Expire[TIME] =
    (state,time) => this.apply(state,time) && other.apply(state,time)

  def ||(other: Expire[TIME]): Expire[TIME] =
    (state,time) => this.apply(state,time) || other.apply(state,time)

  def ! : Expire[TIME] =
    (state,time) => !this.apply(state,time)
}

object Expire {
  /** Cache never expires, and never updates */
  def never[TIME](): Expire[TIME] = of(false)

  /** Cache is always expired, and always updates. */
  def always[TIME](): Expire[TIME] = of(true)

  /** A simplified expiration function, that operates on a boolean. */
  def of[TIME](expire: => Boolean): Expire[TIME] = (_,_) => expire
}