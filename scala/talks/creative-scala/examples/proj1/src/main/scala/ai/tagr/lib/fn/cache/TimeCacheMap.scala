package ai.tagr.lib.fn.cache

import ai.tagr.lib.fn.Self
import ai.tagr.lib.fn.f0.Fn
import ai.tagr.lib.fn.f1.fmap.{FMap, MMap}
import ai.tagr.lib.fn.f1.{Fn1, Fn1Var}

/** A cache that expires based on the passage of time.
  * Can be used as a function 'K => V' or Fn1[K,V]
  *
  * @param cacheFn A function that retrieves a new value for the cache.
  * @param timeFn A function that returns the current time.
  * @param expire A function that takes a cached-time & current-time, and returns `true` if expired.
  * @tparam K The type of the key or function-input.
  * @tparam V The type of the value cached and returned.
  * @tparam TIME A type representing time, used to determine if a value is expired.
  */
class TimeCacheMap[K,V,TIME](
  cacheFn: K => V,
  timeFn: Fn[TIME],
  expire: Expire[TIME]
)
  extends FMap[K,V]
  with Self[TimeCacheMap[K,V,TIME]]
{
  override def self: TimeCacheMap[K, V, TIME] = this

  private val _getFn: Fn1Var[K, V] = Fn1.Var(cacheFn)
  override def getFn: Fn1[K, V] = _getFn

  private val _cache: MMap[K,(TIME,V)] = MMap(Map(), getFn.map(withTime), memoize = true)
  override def apply(): Map[K, V] = _cache.view.mapValues(_._2).toMap

  private def withTime(v:V): (TIME, V) = (timeFn(), v)

  /** Retrieves the value for the given key.
    * If the value for that key exists and is not expired, the cached value is returned.  Otherwise, a new value is retrieved and cached.*/
  override def apply(key: K): V = {
    val (time, v) = _cache(key)

    if (expire(time, timeFn())) {
      force(key)
    } else {
      v
    }
  }

  /** Replaces the function used to calculate values to be returned and cached. */
  def setFn(fn: K => V): SelfType = {
    _getFn.set(fn)
    self
  }


  def setCached(kv: (K,V)): SelfType = {
    _cache += kv._1 -> (timeFn(), kv._2)
    self
  }

  /** Forces refresh and update of the given value, ignoring cache.  Returns the newly updated value. */
  def force(key: K): V = {
    _cache.force(key)._2
  }

  def hasKey(key: K): Boolean = _cache.isDefinedAt(key)

  def isExpired(key: K): Boolean = {
    cachedTime(key).forall(time => expire(time, timeFn()))
  }

  def clear(): SelfType = {
    _cache.clear()
    self
  }

  def cache: Map[K,(TIME,V)] = _cache()
  def cached(key:K): Option[(TIME, V)] = cache.get(key)
  def cachedValue(key:K): Option[V] = cached(key).map(_._2)
  def cachedTime(key:K): Option[TIME] = cached(key).map(_._1)
}
