package ai.tagr.lib.fn.f0

/** Operations that can be performed on a `F0` */
trait FnLike[A] extends (() => A) {
  /** ----------------------------- Convert  -----------------------------  */
  def toSome: Opt[A] = Opt.some(apply())
  def toOpt: Opt[A] = Opt.some(apply())

  def toVar: Var[A] = Var(apply())

  /** An immutable value.  Behaves similar to a val, and Immediately retrieve and store function output */
  def toVal: Fn[A] = {
    val _val = apply()
    Fn(_val)
  }

  /** Store output on first `apply()` */
  def toLazy: Fn[A] = {
    lazy val _lazy = apply()
    Fn(_lazy)
  }

  /** ----------------------------- Map, FlatMap, Filter, etc  -----------------------------  */
  def map[B](f: A=>B): Fn[B] = Fn(f(apply()))

  def is[A1 >: A](elem: A1): Boolean = apply() == elem
  def collectFirst[B](pf: PartialFunction[A, B]): Opt[B] = toSome.collect(pf)
}
