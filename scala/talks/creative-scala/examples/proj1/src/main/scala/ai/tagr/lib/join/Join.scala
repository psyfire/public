package ai.tagr.lib.join



/** Take two sets, and join them based on a given key that matches in both sets.  Use `outer` `inner` to access the joined pairs. */
case class Join[KEY,A,B](a: Seq[A], b: Seq[B], aKey: A => KEY, bKey: B => KEY) {
  val aMap: Map[KEY, Seq[A]] = a.groupBy(aKey)
  val bMap: Map[KEY, Seq[B]] = b.groupBy(bKey)
  val allKeys: Set[KEY] = (aMap.keys ++ bMap.keys).toSet

  private implicit class MapTo[A](seq: Iterable[A]) {
    def mapTo[B](f: A=>B): Map[A,B] = seq.map(a=> a->f(a)).toMap
  }

  lazy val join: Map[KEY, Seq[Joined2[A,B]]] = {
    allKeys.mapTo(k => {
      val aVals = aMap.get(k)
      val bVals = bMap.get(k)
      (aVals, bVals) match {
        case (Some(aV), Some(bV)) => for {ai <- aV; bi <- bV} yield Joined2.ab(ai,bi)
        case (Some(aV), None)     => for {ai <- aV} yield Joined2.a(ai)
        case (None, Some(bV))     => for {bi <- bV} yield Joined2.b(bi)
        case _ => throw new IllegalArgumentException("impossible")
      }
    })
  }

  /** An outer-join.  Will produce pairs, with one item empty, if there are no matches in the other set. */
  lazy val outer: Map[KEY, Seq[Joined2[A, B]]] = join
  lazy val outerValues: Seq[Joined2[A, B]]= outer.values.flatten.toSeq

  /** An inner-join.  Items must have matching keys in both sets. */
  lazy val inner: Map[KEY, Seq[(A, B)]] = join.map{case (k,v)=> k -> v.flatMap(_.both)}
  lazy val innerValues: Seq[(A, B)]= inner.values.flatten.toSeq
}

object Join{
  def apply[A,B](a: Seq[A], b: Seq[B]): PartialJoin[A,B] = new PartialJoin(a,b)
}

case class PartialJoin[A,B](a: Seq[A], b: Seq[B]) {
  /** Specify the field/key in A, which matches a field/key in B. */
  def on[KEY](aKey: A => KEY, bKey: B => KEY): Join[KEY,A,B] = Join(a,b,aKey,bKey)
  def apply[KEY](aKey: A => KEY, bKey: B => KEY): Join[KEY,A,B] = on(aKey,bKey)
}


