name := "prj1"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "org.scalactic" %% "scalactic"   % "3.1.2"
libraryDependencies += "org.scalatest" %% "scalatest"   % "3.1.2" % "test"
libraryDependencies += "org.specs2"    %% "specs2-core" % "4.9.3" % "test"