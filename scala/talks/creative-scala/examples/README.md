## Instructions

Open [proj1](../examples/proj1) under ../examples/proj1

In IntelliJ, import the build.sbt.  Start at the tests under  [proj1/src/test/scala/ai/tagr/example/creative](../examples/proj1/src/test/scala/ai/tagr/example/creative)

## Examples

* [Syntax Fun Examples](proj1/src/test/scala/ai/tagr/example/creative/README.md)
* [Join](proj1/src/main/scala/ai/tagr/lib/join/Join.scala) - Take two sets, and join them based on a given key that matches in both sets.  Use `outer` `inner` to access the joined pairs.
* [Fn & Cache](proj1/src/main/scala/ai/tagr/lib/fn) - Everything is a monad!
* [Check Permissions Brainstorming](CheckPermissionBrainstorm.md)

## Bonus

If we have time, we might dive into some `Tagr.ai` code.

## Disclaimer

These examples are intended to show what is possible.  They're not meant to be examples of clean-code, proper scala practices, or code that won't annoy coworkers.